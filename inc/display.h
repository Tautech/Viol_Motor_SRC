#ifndef __DISPLAY_H_
#define __DISPLAY_H_

#include "stm32f4xx.h"
#include "main.h"

#define X_Dividen	4
#define Y_Dividen	4
#define GRID 	(180/Y_Dividen)
#define SUBTextGaP	26

#ifndef TFT_43
#define	DISP_WIDTH				(480)
#define	DISP_HEIGHT			(272)

#else
#define	DISP_WIDTH				(800)
#define	DISP_HEIGHT			(480)
#endif

#define	BOX_XSIZE				(200)
#define	BOX_YSIZE				(80)

#define	BUTTON_SIZE			(100)
#define	BUTTON_XSIZE			(100)
#define	BUTTON_YSIZE			(80)


#define	nTotal_X				(50 + BUTTON_XSIZE )// 170 ~370
#define	nTotal_Y				(50)// 50 ~130
#define	nCount_X				(nTotal_X)
#define	nCount_Y				(240+50)
#define	nFail_X					(400+100)
#define	nFail_Y					(240+50)
#define	nStart_X				( nTotal_X + BOX_XSIZE +100)
#define	nStart_Y				( 45 )

#define	FAIL_ICON_X			(350)
#define	FAIL_ICON_Y			(200)


#define	BUTTON_UP_X			(50)  // 50 ~ 150
#define	BUTTON_DOWN_X		(BOX_XSIZE + BUTTON_XSIZE +50)
#define	BUTTON_MENU_X			(400)
#define	BUTTON_ENTER_X		(BUTTON_XSIZE*2 + BOX_XSIZE +150 )  // 220 ~320

#define	BUTTON_UP_Y			(nTotal_Y) // 50~130
#define	BUTTON_DOWN_Y		(nTotal_Y)
#define	BUTTON_MENU_Y			(100)
#define	BUTTON_ENTER_Y		(40) // 160~ 240



enum{
	MENU_READY		= 0x0000,
	MENU_SOUND_CHECK = 0x0010,	
	MENU_SOUND_FAIL	= 0x0011,
	MENU_SOUND_OK	= 0x0012,
	MENU_MEASURE		= 0x0020,
	MENU_MEASURE_FINISH = 0x0021,		
	MENU_TOTAL 		= 0x0030,		

};



int 	Display_LCD(void);
void  Display_Logo(void);
void Display_Ready( void );
void Display_SoundCheck_Frame( void );
void Display_SoundCheck( u8 ADDR );
void Display_Window( void );
void  Display_Measure(void);
void	Display_Measure_Frame(void);

void	Display_Menu_Frame(u16	Mode);

void Display_Total(void);
void Display_Total_Button(void );
void Disp_EchoGraph(unsigned char *pData, unsigned short  color);
void Display_Button( void );
void DisplayTouchPoint( void );
void Display_Sound_Error( u8 nResult );
#endif
