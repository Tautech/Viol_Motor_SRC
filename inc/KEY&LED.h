#ifndef _KEY_LED_H
#define _KEY_LED_H

#include <stdio.h>
#include "stm32f4xx.h"

#define LED1_Port						GPIOH
#define LED1_Pin							GPIO_Pin_2
#define LED1_RCC_AHBPeriph				RCC_AHB1Periph_GPIOH
#define	LED1_ON						GPIO_SetBits(LED1_Port,LED1_Pin)
#define	LED1_OFF						GPIO_ResetBits(LED1_Port,LED1_Pin)


#define LED2_Port						GPIOH
#define LED2_Pin						GPIO_Pin_3
#define LED2_RCC_AHBPeriph				RCC_AHB1Periph_GPIOH
#define	LED2_ON						GPIO_SetBits(LED2_Port,LED2_Pin)
#define	LED2_OFF						GPIO_ResetBits(LED2_Port,LED2_Pin)

#define LED3_Port						GPIOI
#define LED3_Pin						GPIO_Pin_8
#define LED3_RCC_AHBPeriph				RCC_AHB1Periph_GPIOI

#define LED4_Port						GPIOI
#define LED4_Pin						GPIO_Pin_10
#define LED4_RCC_AHBPeriph				RCC_AHB1Periph_GPIOI


void JOYState_LED_GPIO_Init(void);
uint8_t Read_JOYState(void);
void Led_Toggle(uint8_t key);
void Delay(__IO uint32_t nCount);
#endif /*_KEY_LED_H*/
