#ifndef __PROTOCOL_H__
#define __PROTOCOL_H__

#include 	"Display.h"
#include "Stm32f4xx.h"

#define	ISTEC

#ifdef ISTEC
#define	STX			'F'  //46
#define	STX1		'M'	//4//4D
#define	STX2		'C'	//43
#define	STX3		'W'	//57
#define	STXS		0x57434D46	//0x464D4357

#else

#endif
#define	ETX			0xF5
#define 	CR			0x0D				
#define 	LF			0x0A
#define	RESET		0x00
//#define	u32			unsigned int

#define MSGID_CRC_LEN   	3
#define CRC_LEN               	2
#define MAX_SUB_LEN       	512
#define DW_DATA_LEN		2048
#define UL_DATA_LEN		1024

#define GUI_STARTDELLEN            		4
#define GUI_ENDDELLEN              		1
#define GUI_HEADLEN                		4
#define GUI_INXCMD                 		4
#define GUI_INXLEN                 		6
#define GUI_INXUSERDATA            		8
#define GUI_CMD_TO_SUBDATA_SIZE   	4
#define GUI_CRC_LEN           			2
#define GUI_RESULT_LEN             		1
#define GUI_MAX_SUB_LEN            		512

//#define RX_BUFFER_SIZE (1024*2)+32
#define 	RX_BUFFER_SIZE (1024)

#define 	LOW			1
#define 	OVER		2
#define	ECHO_PAGE	20	
#define	ECHO_NUM	(232)

#define   PAGE_MAX	(400)
#define	MAX_PAGE	(310)

enum{
	PROTOCOL_SYNC,	
	PROTOCOL_SYNC1,	
	PROTOCOL_SYNC2,	
	PROTOCOL_SYNC3,	
	PROTOCOL_SYNC4,	
	PROTOCOL_CMD,
	PROTOCOL_CMD2,
	PROTOCOL_LENGTH_H,	
	PROTOCOL_LENGTH_L,	
	PROTOCOL_DATA,		
	PROTOCOL_CRC,		
	PROTOCOL_ETX,		


};

// ====== Error Defien======
#define	NO_ERROR				b00000000
#define	NO_SIGNAL				b00000001
#define	SIGNAL_HUNTING			b00000010
#define	TEMPERATURE_ERROR		b00000100
#define	COMM_ERROR			b00001000
#define	NO_SENSOR_ERROR		b00011000

//==== Command Define ====
#define _Sensor1DataGet			0x0011
#define _Sensor1EchoGet			0x0012
#define _Sensor1DataGet_rp		0x0021
#define _Sensor1EchoGet_rp		0x0022
#define _Sensor1Parameter		0x0013
#define _Sensor1Parameter_rp	0x0023
#define _Sensor1ParameterSet	0x0014
#define _Sensor1VersionGet		0x0015
#define _Sensor1Version_rp		0x0025

#define _Sensor2DataGet			0x0031
#define _Sensor2EchoGet			0x0032
#define _Sensor2DataGet_rp		0x0041
#define _Sensor2EchoGet_rp		0x0042

/*
//===== FMCW Command Define
#define _Sensor1DataGet			0x0011
#define _Sensor2DataGet			0x0012
#define _Sensor1DataGet_rp		0x0021
#define _Sensor2DataGet_rp		0x0022

#define	_Sensor1Parameter		0x0031
#define	_Sensor1Parameter_rp		0x0041
#define	_Sensor2Parameter		0x0032
#define	_Sensor2Parameter_rp		0x0042
*/

#define	ZERO_DISTANCE	(0.3)

#ifdef KEIL_V4
	#pragma pack(push,1)
#else	// IAR
//	#pragma pack(1)
	#pragma pack(push,1)
#endif




typedef struct CursorParameter{
	u16		nStartCursorPos;
	u16		nEndCursorPos;
	u16		nCurrentPos;
	u16		nDecimalPos;

}TCursorParameter;

typedef struct System{


	u8		nLastStatus, nStatus;
	
	u16 		nMenu;
	u16 		nLastMenu;
	u16		nDisplayMode,nLastDisplayMode;

	Flo32	fTempValue;
	s32		nTempInt;		
	u32		nTempWord;
	u16		nTempValue;
	u8		nTempByte;

	Flo32	fFREQ, fOffset, fDelta, fdBValue[2];
	u16		nIndex[2];
	u16		nIndexMax[2], nDelay;
	Flo32	fdBMax[2];
	u8		nButton;
	u8		nTempButton;
	u32		nCNT, nCheckCNT;
	
	_Bool	OnRefresh;
	_Bool 	nSoundOK;		// System Parameter Value Editing


}TSystem;


extern TSystem System;

#ifdef KEIL_V4 
#pragma pack(pop)
#endif
#pragma pack(pop)
 


#endif


