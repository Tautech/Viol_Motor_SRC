//////////////////////////////////////////////////////////////////////////////////	 

/**************************************************************************************************/		
 /* @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, QD electronic SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
**************************************************************************************************/	
#ifndef __LCD_H
#define __LCD_H		
//#include "sys.h"	 
#include "stdlib.h"
#include "main.h"
#include "stm32f4xx.h"
#include "Rgb_color_table.h"


#define LCD_REG              (*((volatile u16 *) 0x60000000)) /* RS = 0 *///A18		// (*((volatile unsigned short *) 0x6F000000)) /* RS = 0 */ A15
#define LCD_RAM              (*((volatile u16 *) 0x60080000)) /* RS = 1 *///A18		// (*((volatile unsigned short *) 0x6F010000)) /* RS = 1 */ A15


/* LCD color */
#define White          RGB_WHITE
#define Black           RGB_BLACK
#define Grey            RGB_GRAY
#define Blue            RGB_BLUE
#define Red             RGB_RED
#define Magenta      RGB_MAGENTA
#define Green          RGB_GREEN
#define Cyan           RGB_CYAN
#define Yellow         RGB_YELLOW

#define 	GET_ADDRESS_MODE	0x0B
#define	GET_LCD_MODE		0xB1

#define	SET_PARTIAL_MODE	0x12
#define	SET_NORMAL_MODE	0x13
#define	SET_DISPLAY_OFF	0x28
#define	SET_DISPLAY_ON		0x29
#define	SET_COLUMN_ADDR	0x2A
#define	SET_PAGE_ADDR		0x2B
#define 	WRITE_MEM_START	0x2C
#define	READ_MEM_START	0x2E
#define	SET_PARTIAL_AREA	0x30
#define	SET_ADDR_MODE		0x36
#define	SET_NORMAL_DISPLAY	0x00
#define	SET_REVARSAL_DISPLAY	0x03
#define	SET_PIXEL_FORMAT	0x3A
#define	GET_PIXEL_FORMAT	0x0C
#define	SET_LCD_MODE		0xB0
#define	SET_HOR_PERIOD	0xB4
#define	SET_VERT_PERIOD	0xB6
#define	SET_GPIO_CONF		0xB8
#define 	SET_GPIO_VALUE		0xBA
#define	SET_PWM_CONF		0xBE
#define 	SET_DBC_CONF		0xD0
#define 	SET_DBC_TH			0xD4
#define 	START_PLL			0xE0
#define 	SET_PLL_MN			0xE2
#define	SET_LSHIFT_FREQ		0xE6
#define	SET_PIXEL_INTERFACE	0xF0
#define	GET_PIXEL_INTERFACE	0xF1
#define	nFONT12X24_SIZE	(36)			// in Byte
#define   nFONT12x24WIDTH	(12)
#define   nFONT12x24HEIGHT	(24)
#define   nFONT12x24PAGE	(3)

#define	NORMAL				(0)
#define	TRANSPARENT		(1)

	    															  
void LCD_Initializtion(void);
void LCD_DisplayOn(void);
void LCD_DisplayOff(void);
void LCD_Clear(u16 Color);	 
void LCD_SetCursor(u16 Xpos, u16 Ypos);
void LCD_DrawPoint(u16 x,u16 y, u16 Color);
u16  LCD_ReadPoint(u16 x,u16 y); //����
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2);
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2);		   
void LCD_SetWindows(u16 xStar, u16 yStar,u16 xEnd,u16 yEnd);

u16 LCD_RD_DATA(void);//��ȡLCD����									    
void LCD_WriteReg(u8 LCD_Reg, u16 LCD_RegValue);
void LCD_WR_DATA(u8 data);
u16 LCD_ReadReg(u8 LCD_Reg);
void LCD_WriteRAM_Prepare(void);
void LCD_WriteRAM(u16 RGB_Code);
u16 LCD_ReadRAM(void);		   
u16 LCD_BGR2RGB(u16 c);
void LCD_SetParam(void);
void Lcd_WriteData_16Bit(u16 Data);
void LCD_direction(u8 direction );

void LCD_Color(u16 xStart, u16 yStart, u16 xSize, u16 ySize, u16 color);

void LCD_SetAddr(u16 x1, u16 y1, u16 x2, u16 y2);	
void LCD_Draw_xline(u16 x1, u16 y1, short dist, u16 color);
void LCD_Draw_yline(u16 x1, u16 y1, short dist, u16 color);
void LCD_Draw_box(u16 x1, u16 y1, u16 Width, u16 Height, u16 color);
void LCD_DrawGraph( u16 x0, u16 y0,  u16 y1 , u16 color );

void LCD_Show_Image(u16 xsta, u16 ysta, u16 xsize, u16 ysize, u16 *image ,u8 MODE);
void	LCD_Read_Image(u16 Left, u16 Top, u16 Width,u16  Height,u16 *img );
void LCD_String_Write(u16 xPos,u16 yPos,char *ch,u16 size, u16 Height, u16 text_color, u16 back_color, u16 MODE);
void  LCD_Char_Write(u16 xPos,u16 yPos,u8 nIndex,u16 size,u16 Height,u16 Text_color, u16 Back_color,  u16 MODE);		

void Delay_ms(uint32_t ms) ;   

#endif  
	 
	 



