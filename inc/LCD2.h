/*********************************************************************************************************
*
* File                : LCD.h
* Hardware Environment: 
* Build Environment   : RealView MDK-ARM  Version: 4.20
* Version             : V1.0
* By                  : 
*
*                                  (c) Copyright 2005-2011, WaveShare
*                                       http://www.waveshare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#ifndef __GLCD_H 
#define __GLCD_H

/* Includes ------------------------------------------------------------------*/
#include <math.h>
#include "stm32f4xx.h"
#include "AsciiLib.h" 

/* Private define ------------------------------------------------------------*/
extern uint16_t DeviceCode;
#define USE_16BIT_PMP

/*********************************************************************
* Overview: Horizontal and vertical display resolution
*                  (from the glass datasheet).
*********************************************************************/
//#define DISP_HOR_RESOLUTION				320
//#define DISP_VER_RESOLUTION				240
//如果是A屏(SSD1289驱动) 则选择DISP_ORIENTATION 
//#define DISP_ORIENTATION					0
#define DISP_ORIENTATION					90
//#define DISP_ORIENTATION					180
//#define DISP_ORIENTATION					270

/* Private define ------------------------------------------------------------*/

#if  ( DISP_ORIENTATION == 90 ) || ( DISP_ORIENTATION == 270 )

#define  MAX_X  320
#define  MAX_Y  240   

#elif  ( DISP_ORIENTATION == 0 ) || ( DISP_ORIENTATION == 180 )

#define  MAX_X  240
#define  MAX_Y  320   

#endif

#define	SET_DISPLAY_OFF	0x28
#define	SET_DISPLAY_ON		0x29
#define	SET_COLUMN_ADDR	0x2A
#define	SET_PAGE_ADDR		0x2B
#define 	WRITE_MEM_START	0x2C
#define 	READ_MEM_START	0x2E
#define	SET_ADDR_MODE		0x36
#define	SET_PIXEL_FORMAT	0x3A
#define	SET_LCD_MODE		0xB0
#define	SET_HOR_PERIOD	0xB4
#define	SET_VERT_PERIOD	0xB6
#define	SET_GPIO_CONF		0xB8
#define 	SET_GPIO_VALUE		0xBA
#define 	SET_DBC_CONF		0xD0
#define 	SET_DBC_TH			0xD4
#define 	START_PLL			0xE0
#define 	SET_PLL_MN			0xE2
#define	SET_LSHIFT_FREQ		0xE6
#define	SET_PIXEL_INTERFACE	0xF0
#define	SET_NORMAL_DISPLAY	0x00
#define	SET_REVARSAL_DISPLAY	0x03

#define	nFONT12X24_SIZE	(36)			// in Byte
#define   nFONT12x24WIDTH	(12)
#define   nFONT12x24HEIGHT	(24)
#define   nFONT12x24PAGE	(3)
#define	TRANSPARENT	(1)

/*********************************************************************
* Overview: Horizontal synchronization timing in pixels
*                  (from the glass datasheet).
*********************************************************************/
//#define DISP_HOR_PULSE_WIDTH		20    /* 20 */
//#define DISP_HOR_BACK_PORCH			51	  /* 48	*/
//#define DISP_HOR_FRONT_PORCH		20	  /* 20 */

/*********************************************************************
* Overview: Vertical synchronization timing in lines
*                  (from the glass datasheet).
*********************************************************************/
//#define DISP_VER_PULSE_WIDTH		2	  /* 2 */
//#define DISP_VER_BACK_PORCH			12	  /* 16 */
//#define DISP_VER_FRONT_PORCH		4	  /* 4 */

/*********************************************************************
* Definition for SPI interface for HIMAX 8238-A relevant to hardware 
* layout; Hardware dependent!
*********************************************************************/
#define GPIO3 3
#define GPIO2 2
#define GPIO1 1
#define GPIO0 0
#define LCD_RESET (1<<GPIO3)	   /* LCD Reset signal (Reset for display panel, NOT ssd1963) */
#define LCD_SPENA (1<<GPIO0)	   /* SPI EN signal */
#define LCD_SPCLK (1<<GPIO1)	   /* SPI CLK */
#define LCD_SPDAT (1<<GPIO2)	   /* SPI DATA */

/* LCD color */
#define White          0xFFFF
#define Black          0x0000
#define Grey           0xF7DE
#define Blue           0x001F
#define Blue2          0x051F
#define Red            0xF800
#define Magenta        0xF81F
#define Green          0x07E0
#define Cyan           0x7FFF
#define Yellow         0xFFE0

#define RGB565CONVERT(red, green, blue) (int) (((red >> 3) << 11) | ((green >> 2) << 5) | (blue >> 3))

/* Private function prototypes -----------------------------------------------*/
void LCD_Initializtion(void);
void LCD_Clear(uint16_t Color);	
//void LCD_SetBacklight(uint8_t intensity);
uint16_t LCD_GetPoint(uint16_t Xpos,uint16_t Ypos);
void LCD_SetPoint(uint16_t Xpos,uint16_t Ypos,uint16_t point);
void PutChar(uint16_t Xpos,uint16_t Ypos,uint8_t c,u8 nWidth, uint16_t charColor,uint16_t bkColor);
void LCD_DrawLine( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1 , uint16_t color );
void PutChinese(uint16_t Xpos,uint16_t Ypos,uint8_t *str,uint16_t Color,uint16_t bkColor); 
void GUI_Text(uint16_t Xpos, uint16_t Ypos, uint8_t *str, u8 nWidth,uint16_t Color, uint16_t bkColor);
void GUI_Chinese(uint16_t Xpos, uint16_t Ypos, uint8_t *str,uint16_t Color, uint16_t bkColor);	
void LCD_DrawPicture(uint16_t StartX,uint16_t StartY,uint16_t EndX,uint16_t EndY,uint16_t *pic);


void LCD_WriteIndex(uint16_t index);
void LCD_WriteData(uint16_t data);
uint16_t LCD_ReadData(void);
uint16_t LCD_ReadReg(uint16_t LCD_Reg);
void LCD_WriteReg(uint16_t LCD_Reg,uint16_t LCD_RegValue);
static void LCD_SetCursor( uint16_t Xpos, uint16_t Ypos );
void delay_ms(uint16_t ms);

void LCD_SetAddr(u16 x1, u16 y1, u16 x2, u16 y2);	
void LCD_Color( u16 xStart, u16 yStart, u16 xSize, u16 ySize, u16 color);

void  LCD_String_Mode(u32 xPos,u32 yPos,char *ch,u16 size, u16 text_color, u16 back_color, u16 MODE);
void	 Read_Image(u16 Left, u16 Top, u16 Width,u16  Height,u16 *img );
void  LCD_char_mode(u32 xPos,u32 yPos,u32 nIndex,u16 size,u16 Text_color, u16 Back_color,  u16 MODE);

void  LCD_String_Write(u16 xPos,u16  yPos,char *ch,u16 size, u16  Height, u16  text_color, u16 back_color, u16 MODE);  // Size = 12
void  LCD_Char_Write(u16  xPos,u16  yPos,char nIndex,u16 size,u16 Height,u16 Text_color, u16 Back_color,  u16 MODE); // size = 12
void LCD_DrawGraph( uint16_t x0, uint16_t y0,  uint16_t y1 , uint16_t color );
#endif 

/*********************************************************************************************************
      END FILE
*********************************************************************************************************/

