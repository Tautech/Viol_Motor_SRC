#ifndef _KEY_PROCESS_H
#define _KEY_PROCESS_H


//-------- KEYPAD  PORT --------
#define KEYPAD_PORT		GPIOC

#define BUTTON0	GPIO_Pin_0
#define BUTTON1	GPIO_Pin_1
#define BUTTON2	GPIO_Pin_2
#define BUTTON3	GPIO_Pin_3
#define BUTTON4	GPIO_Pin_4
#define BUTTON5	GPIO_Pin_5
#define BUTTON6	GPIO_Pin_6
#define BUTTON7	GPIO_Pin_7 




#define ESC 		b11111110 //0xFE		
#define MENU		b11111101
#define CLN		b11111011
#define UP		b11110111
#define ENTER	b11011111
#define LEFT		b10111111
#define DOWN	b01111111
#define RIGHT	b11101111
#define NO_KEY	b11111111

#if 0
#define KEY_ECHO	0xFB
#define KEY_TREND	0xFE
#define KEY_MENU	0xFD
#define KEY_SIGNAL	0xFB
#define KEY_UP		0xF7
#define KEY_RIGHT 	0x7F
#define KEY_DOWN 	0xBF
#define KEY_LEFT 	0xDF
#define KEY_ENTER	0xEF
#define KEY_NO		0xFF

#endif

enum{

	BUTTON_BLANK	= 	0x00, 
	BUTTON_TOTAL	= 	0x01, 
	BUTTON_UP		=	0x02,
	BUTTON_DOWN	=	0x03,
	BUTTON_ENTER	= 	0x04, 
	BUTTON_START	= 	0x05,
	BUTTON_FAIL	= 	0x06,
	BUTTON_OK		= 	0x07,

};

enum{
	BUTTON_OPEN	 	= 0x00,
	BUTTON_PRESSED	= 0x01,

};

enum{
	LINEAR,
	SQUARE,
	CUBIC,
};

#define	RANGE_MAX		(40)
#define	METER_TO_INDEX	(8)
#define	METER_TO_FEET	(1/0.3048)

void keypad_init(void);
_Bool Key_Check(void);
void key_process(void);
void KeyHiddenProcess(void);
void KeyReadyProcess(void);
void KeyMeasureProcess(void);
void KeyTotalProcess(void);
void KeySoundCheckProcess( void );
void KeySoundFailProcess( void );
void KeySoundOKProcess( void );

void SetCurrentPosition(int nStartPos,int nEndPos,int nCurrPos,int nJumPos);
void MoveCurrentPosition(int nDirection);

#endif
