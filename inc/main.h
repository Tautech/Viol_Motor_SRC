
#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "stm32f4xx.h"

#define TFT_43
#define	PI			( 3.1415926535897932384626 )
#define	CNT_1msec	1000
#define	CNT_200msec	200
#define	CNT_500msec	500

#define ADC_CDR_ADDRESS    ((uint32_t)0x40012308)
#define  ADC_BUFFER_SIZE		( 4096 )//1024
#define	ADC_FFT_SIZE			2048
#define	DECI				(8 )
#define	MAXTEST			(1000 )
#define	CHECK_NUM			(6)
#define	REFERENCE_LEVEL	(150)
#define	TEXT_START			(550)

extern u8		dBData[ADC_BUFFER_SIZE/2];

typedef struct MotorParam{
	u8		nResult[2];
	u16		nMaxIndex[2], nMinIndex[2], nError[2],nErrorCNT, nCNT, nTotal, nCheckIndex[2][CHECK_NUM];
	Flo32	fAvgValue[2],fAvgIndex[2];
	Flo32	fdBValue[2][MAXTEST];
	Flo32	fSTDDEV[2];
	Flo32	fCheckValue[2][CHECK_NUM];

}TMotorParam;

extern	TMotorParam Motor;


typedef enum{
	CHECK,
	MEASURE,
	MEASURE_READY,
	RELAY_ON,
	DELAY_10msec,
	ADC1_Input,
	RELAY_OFF,
	DELAY_150msec,
	ADC2_Input,
	DELAY_1msec,
	FFT1_START,
	ANALYSIS1,
	FFT2_START,
	ANALYSIS2,
	CHECK_SOUND,
	DELAY_1500msec,
	CYCLE_DONE,
	ALL_DONE,
	DISPLAY_DATA,
	DISPLAY_DELAY,
} TMeasureStep;

extern TMeasureStep  MotorTest;


/* Private typedef -----------------------------------------------------------*/
//typedef enum {FAILED = 0, PASSED = !FAILED} bool;

#define FLASH_START_ADDR   ((uint32_t)0x08004000)  /* Start @ of System Parameter Flash area */
#define FLASH_END_ADDR     ADDR_FLASH_SECTOR_8   /* End @ of user Flash area */
#define FLASH_WRITTEN_BY_USER	0x20030307			// System Parameter ID 
#define FLASH_ECHO_ADDR	 ((uint32_t)0x0800C000)    /* Start @ of Trend Flash area */

#define	FLASH_TEMP_ADDR			((uint32_t)0x080A0000)
#define	FLASH_LOG_HEADER_ADDR	((uint32_t)0x080C0000)
#define	FLASH_TREND_ADDR		 	((uint32_t)0x080C0010)


#define FLASH_PASSWORD		0x19690118
#define FLASH_PASSWORD_ADDR	((uint32_t)0x08008000) /* Base @ of Sector 1, 16 Kbytes */
/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */


#define	DAC_PORT		GPIOB
#define	DAC_DIN		GPIO_Pin_12
#define	DAC_SCLK		GPIO_Pin_13
#define	DAC_SYNC		GPIO_Pin_14

#define	ERROR_RELAY	GPIO_Pin_0
#define	HIGH_ALARM	GPIO_Pin_1
#define	LOW_ALARM		GPIO_Pin_2
#define	RELAY3			GPIO_Pin_3
#define	RELAY4			GPIO_Pin_4

#define LCD_RCC_SPI   	        		RCC_APB1Periph_SPI2
#define LCD_GPIO_AF_SPI 				GPIO_AF_SPI2

#define LCD_SPI                        SPI2
#define LCD_SPI_CLK_INIT               RCC_APB1PeriphClockCmd
#define LCD_SPI_IRQn                   SPI2_IRQn

#define LCD_SPI_SCK_PIN                		GPIO_Pin_13
#define LCD_SPI_SCK_GPIO_PORT          	GPIOB
#define LCD_SPI_SCK_GPIO_CLK           	RCC_AHB1Periph_GPIOB
#define LCD_SPI_SCK_SOURCE             	GPIO_PinSource13

#define LCD_SPI_MISO_PIN               		GPIO_Pin_2
#define LCD_SPI_MISO_GPIO_PORT         	GPIOC
#define LCD_SPI_MISO_GPIO_CLK         	RCC_AHB1Periph_GPIOC
#define LCD_SPI_MISO_SOURCE            	GPIO_PinSource2

#define LCD_SPI_MOSI_PIN               		GPIO_Pin_15
#define LCD_SPI_MOSI_GPIO_PORT         	GPIOB
#define LCD_SPI_MOSI_GPIO_CLK          	RCC_AHB1Periph_GPIOB
#define LCD_SPI_MOSI_SOURCE           	GPIO_PinSource15		


#define LCD_SPI_NSS_PIN				GPIO_Pin_12
#define LCD_SPI_NSS_GPIO_PORT			GPIOB
#define LCD_SPI_NSS_GPIO_CLK			RCC_AHB1Periph_GPIOB
#define LCD_SPI_NSS_SOURCE			GPIO_PinSource12

#define LCD_RESET_PIN				GPIO_Pin_1
#define LCD_RESET_GPIO_PORT		GPIOC
#define LCD_RESET_RCC_CLK	RCC_AHB1Periph_GPIOC
#define LCD_RESET_SOURCE			GPIO_PinSource1
enum{
	FFT,			
	IFFT,			
};	
#if 1
#define	ADC_PORT				GPIOC
#define	ADC_Pin					GPIO_Pin_2
#define	ADC_CHANNEL			ADC_Channel_12
#define	RCC_AHB1Periph_PORT	RCC_AHB1Periph_GPIOC
#else

#define	ADC_PORT				GPIOC
#define	ADC_Pin					GPIO_Pin_2
#define	ADC_CHANNEL			ADC_Channel_12
#define	RCC_AHB1Periph_PORT	RCC_AHB1Periph_GPIOC
#endif



#define		UART_DEBUG
#define START_PIN                         	GPIO_Pin_5
#define START_GPIO_PORT                 GPIOE
#define START_GPIO_CLK                   RCC_AHB1Periph_GPIOE  

#define	START_ON					GPIO_SetBits(START_GPIO_PORT,START_PIN)
#define	START_OFF				GPIO_ResetBits(START_GPIO_PORT,START_PIN)

#define LED_PIN                         	GPIO_Pin_12
#define LED_GPIO_PORT                 GPIOD
#define LED_GPIO_CLK                   RCC_AHB1Periph_GPIOD  

#define	LED_ON					GPIO_SetBits(LED_GPIO_PORT,LED_PIN)
#define	LED_OFF				GPIO_ResetBits(LED_GPIO_PORT,LED_PIN)
#define	LED_TOGGLE				GPIO_ToggleBits(LED_GPIO_PORT,LED_PIN)

#define USER_BUTTON_PIN                GPIO_Pin_0
#define USER_BUTTON_GPIO_PORT          GPIOA
#define USER_BUTTON_GPIO_CLK           RCC_AHB1Periph_GPIOA

extern u16	nTotal, nCount, nFail;
extern u8		nMenu;
extern u8		nButton;
extern u8		nLCD_Mode[7];

void GPIO_Config(void);
void EXTI_Config(void);
void EXTI4_IRQHandler(void);
void System_Periodic_Handle(__IO uint32_t localtime);
void Time_Update(void);
static void NVIC_Config(void);
static void SysTick_Configuration(void);

void TimingDelay_Decrement(void);
void LED_init(void);
void SystemVariables_init(void);
void Save_Log_Data(void);
uint32_t GetSector(uint32_t Address);	
void Flash_ReadData(void);
void Flash_WriteData(void);

void Flash_Read_Header(void);
void Flash_Write_Trend(void);
char Check_PassWord(void);

void My_Interpolation( u8 *dst, u8 *src, u16 xStart, u16 xEnd, double Coef, char METHOD);
void My_Interpolation2( u8 *dst, u8 *src );
void My_Interpolation3( u8 *dst, u8 *src );
void Alarm_Process(void);
void Current_Setting(void);
void SetDAC ( unsigned short nData );
void SetDAC7512(unsigned short nData);
unsigned short CalculateDac(void);
void SetAlarmStatus(void);

int	  max(int value1, int value2);
int	  min(int value1, int value2);

void TIM_Config(void);
void PWM_SETTING(uint32_t PERIOD);
void TimeCheck(void);
void Delay_us(volatile u32 usec);

u8 Check_Touch( void );
void ADC_Single_Config_ADC1(void);
u8 ADC_INPUT( u8 ADDR );
u8 Display_Data( void );
u8 FFT_CONV(u8 ADDR );
u8 CheckMotor( u8 ADDR );
void SetupMotor( u8 ADDR );
void Calcu_STD_Dev( void );
void Measure_Motor( void );
u8 CheckSound( void );
void FindFreqSound( void );
void Init_Parameter( void );
#endif


