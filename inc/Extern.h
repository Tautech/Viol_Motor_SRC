#ifndef __EXTERN_H__
#define __EXTERN_H__	

extern float Temperature_Degree;
extern unsigned char MenuCurrentStatus;

extern unsigned char press_key;


// USART1 Parameters
extern u16 UpArrow[8000] ;
extern u16 DownArrow[8000] ;
extern u16 MenuIcon[6000];
extern u16 EnterIcon[6000];
extern u16 Enter3Icon[10000];
extern u16 YellowButton[16000];
extern u16 StartButton[8100];
extern u16 StopButton[8100];
extern u16 SoundError[10000];
extern u16 SoundOK_Green[10000];

extern u16 Feet_picture[8192];
extern u16 Meter_picture[8192];
extern u16 Percent_picture[8192];
extern u16 Liter_picture[8192];
extern u16 Centi[8192];
extern u16 Centi_Picture[12288];
extern u16 Back_Color;


extern u16 Hexagon[2500];

extern u16 MenuBackGround[272];
extern u16 MenuButton[100];
extern u16 SubMenuBack[30];
extern u16 SubItemGap[180];

extern u16 SubMenuLeft[270];
extern u16 SubMenuLeft3[270];
extern u16 SubMenuRight[270];

extern u16 IconSystem[1600];
extern u16 IconAlarm[1600];
extern u16 IconTrim[1600];
extern u16 IconSwitch[1600];
extern u16 IconVersion[1600];
extern u16 IconShape[1600];

extern u16 Separator[6];
extern u16 Separator2[12];	
extern u16 SeparatorTop[3];
extern u16 SeparatorBottom[3];
extern u16 FOWIN[38400];
extern Flo32 Blackman2048[2048];
extern Flo32 Blackman_Real[2048];
extern Flo32 Blackman_Imag[2048];
extern Flo32 BlackmanHarris256[256];
extern Flo32 LPF16[16];
extern Flo32 LPF24[24];
extern Flo32 real[4096];
extern Flo32 imag[4096];
/* External variable declarations ----------------------------------------------*/
extern unsigned char  FONT_16x24[95][16*3];
extern unsigned char  FONT_12x24[95][12*3];
extern unsigned char  FONT2_12x24[95][12*3];
extern unsigned char  FONT3_12x24[95][12*3];
extern unsigned char  FONT8x16[96][16];
#endif

