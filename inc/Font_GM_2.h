// File : _LCDFontLib16x16_ascii.C 
// FontMakerLCD : Text mode 
// Copyright <2010.08> G&M Technology Co., Ltd. 
// 

// int		nFontWidht  = 16;	
// int		nFontHeight = 16;	
// 
// Font : "MS Mincho", Scheme = 7, Scale = 3 

// " !"#$%&'()*+,-./0123456789:;<=>?" 
const unsigned char asc4_1616[95][16*2] =
{ 
// " " 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "!" 
{	0x80,0x01,			
	0xC0,0x03,			
	0xC0,0x03,			
	0xC0,0x03,			
	0xC0,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x00,0x00,			
	0xC0,0x01,			
	0xC0,0x03,			
	0xC0,0x01,			
	0x00,0x00},			
// """ 
{	0x00,0x00,			
	0xE0,0x3D,			
	0xF0,0x1E,			
	0x38,0x07,			
	0x8C,0x01,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "#" 
{	0x00,0x00,			
	0x20,0x18,			
	0x30,0x18,			
	0x30,0x18,			
	0x30,0x18,			
	0xFE,0x7F,			
	0xFE,0x7F,			
	0x30,0x08,			
	0x18,0x0C,			
	0x18,0x0C,			
	0xFE,0x7F,			
	0xFE,0x7F,			
	0x18,0x0C,			
	0x18,0x0C,			
	0x08,0x04,			
	0x00,0x00},			
// "$" 
{	0x00,0x00,			
	0x80,0x01,			
	0xE0,0x07,			
	0xF8,0x1F,			
	0x9C,0x1D,			
	0xBC,0x1D,			
	0xF8,0x01,			
	0xE0,0x07,			
	0x80,0x0F,			
	0x80,0x3D,			
	0x9C,0x39,			
	0x9C,0x39,			
	0xBC,0x1F,			
	0xF0,0x07,			
	0x80,0x01,			
	0x00,0x00},			
// "%" 
{	0x00,0x00,			
	0x00,0x30,			
	0x3C,0x38,			
	0x77,0x1C,			
	0x63,0x0C,			
	0x63,0x06,			
	0x67,0x03,			
	0xFE,0x01,			
	0xC0,0x7E,			
	0xE0,0x67,			
	0x60,0xE3,			
	0x30,0xE7,			
	0x18,0x67,			
	0x0C,0x3E,			
	0x04,0x00,			
	0x00,0x00},			
// "&" 
{	0x00,0x00,			
	0x00,0x30,			
	0x3C,0x38,			
	0x77,0x1C,			
	0x63,0x0C,			
	0x63,0x06,			
	0x67,0x03,			
	0xFE,0x01,			
	0xC0,0x7E,			
	0xE0,0x67,			
	0x60,0xE3,			
	0x30,0xE7,			
	0x18,0x67,			
	0x0C,0x3E,			
	0x04,0x00,			
	0x00,0x00},			
// "'" 
{	0x00,0x00,			
	0x3C,0x00,			
	0x3C,0x00,			
	0x38,0x00,			
	0x1C,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "(" 
{	0x00,0x20,			
	0x00,0x78,			
	0x00,0x1C,			
	0x00,0x0E,			
	0x00,0x07,			
	0x80,0x03,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x03,			
	0x00,0x07,			
	0x00,0x0E,			
	0x00,0x1C,			
	0x00,0x70,			
	0x00,0x00},			
// ")" 
{	0x00,0x00,			
	0x0E,0x00,			
	0x38,0x00,			
	0x70,0x00,			
	0xE0,0x00,			
	0xC0,0x01,			
	0xC0,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0xC0,0x01,			
	0xC0,0x01,			
	0xE0,0x00,			
	0x70,0x00,			
	0x38,0x00,			
	0x0E,0x00,			
	0x00,0x00},			
// "*" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0x8E,0x71,			
	0xBE,0x7D,			
	0xF8,0x1F,			
	0xFC,0x3F,			
	0xBE,0x7D,			
	0xC6,0x21,			
	0xC0,0x01,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "+" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0xFE,0x7F,			
	0xFE,0x7F,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "," 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x3C,0x00,			
	0x3C,0x00,			
	0x38,0x00,			
	0x38,0x00,			
	0x0C,0x00},			
// "-" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xFE,0x7F,			
	0xFE,0x7F,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "." 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x18,0x00,			
	0x3C,0x00,			
	0x3C,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "/" 
{	0x00,0x00,			
	0x00,0x60,			
	0x00,0x30,			
	0x00,0x18,			
	0x00,0x1C,			
	0x00,0x0E,			
	0x00,0x07,			
	0x80,0x03,			
	0xC0,0x01,			
	0xE0,0x00,			
	0x70,0x00,			
	0x38,0x00,			
	0x1C,0x00,			
	0x0C,0x00,			
	0x06,0x00,			
	0x00,0x00},			
// "0" 
{	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xF0,0x0F,			
	0x38,0x1C,			
	0x1C,0x38,			
	0x0E,0x78,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x78,			
	0x1C,0x38,			
	0x1C,0x1C,			
	0x70,0x0E,			
	0xC0,0x03,			
	0x00,0x00,			
	0x00,0x00},			
// "1" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x01,			
	0xF8,0x01,			
	0xC0,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0xC0,0x03,			
	0xF8,0x1F,			
	0x00,0x00,			
	0x00,0x00},			
// "2" 
{	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xF8,0x1F,			
	0x0C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x00,0x1E,			
	0x00,0x07,			
	0xC0,0x01,			
	0x70,0x20,			
	0x1C,0x30,			
	0xFE,0x3F,			
	0xFE,0x3F,			
	0x00,0x00,			
	0x00,0x00},			
// "3" 
{	0x00,0x00,			
	0x00,0x00,			
	0xE0,0x03,			
	0xFC,0x1F,			
	0x1C,0x1C,			
	0x1C,0x18,			
	0x00,0x1C,			
	0xC0,0x0F,			
	0x00,0x1E,			
	0x00,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x3C,0x1E,			
	0xF0,0x03,			
	0x00,0x00,			
	0x00,0x00},			
// "4" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x0C,			
	0x00,0x0E,			
	0x80,0x0F,			
	0xC0,0x0F,			
	0x60,0x0E,			
	0x38,0x0E,			
	0x1C,0x0E,			
	0x0E,0x0E,			
	0xFE,0x7F,			
	0x00,0x0E,			
	0x00,0x0E,			
	0xC0,0x7F,			
	0x00,0x00,			
	0x00,0x00},			
// "5" 
{	0x00,0x00,			
	0x00,0x00,			
	0xF8,0x3F,			
	0xF8,0x3F,			
	0x18,0x00,			
	0x08,0x00,			
	0x88,0x07,			
	0xFC,0x1F,			
	0x1C,0x38,			
	0x00,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x3C,0x1E,			
	0xE0,0x03,			
	0x00,0x00,			
	0x00,0x00},			
// "6" 
{	0x00,0x00,			
	0x00,0x00,			
	0x80,0x07,			
	0xF0,0x1F,			
	0x38,0x3C,			
	0x1C,0x00,			
	0x8E,0x07,			
	0xEE,0x3F,			
	0x3E,0x78,			
	0x1E,0x70,			
	0x0E,0x70,			
	0x1C,0x38,			
	0x78,0x1C,			
	0xC0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "7" 
{	0x00,0x00,			
	0x00,0x00,			
	0xF8,0x3F,			
	0xFC,0x3F,			
	0x0C,0x18,			
	0x04,0x0C,			
	0x00,0x06,			
	0x00,0x03,			
	0x80,0x03,			
	0xC0,0x01,			
	0xC0,0x01,			
	0xE0,0x01,			
	0xE0,0x01,			
	0xC0,0x01,			
	0x00,0x00,			
	0x00,0x00},			
// "8" 
{	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xF8,0x1F,			
	0x0C,0x38,			
	0x0C,0x30,			
	0x7C,0x38,			
	0xF8,0x0F,			
	0xB8,0x1F,			
	0x0E,0x3C,			
	0x0E,0x30,			
	0x0E,0x30,			
	0x3C,0x1E,			
	0xE0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "9" 
{	0x00,0x00,			
	0x00,0x00,			
	0xE0,0x03,			
	0xFC,0x1F,			
	0x1E,0x38,			
	0x0E,0x38,			
	0x0E,0x78,			
	0x0E,0x7C,			
	0x3C,0x7F,			
	0xF0,0x3B,			
	0x00,0x38,			
	0x1C,0x1C,			
	0x3C,0x0F,			
	0xF0,0x01,			
	0x00,0x00,			
	0x00,0x00},			
// ":" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xC0,0x03,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xC0,0x03,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// ";" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xC0,0x03,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xC0,0x03,			
	0x80,0x03,			
	0xC0,0x01,			
	0x00,0x00,			
	0x00,0x00},			
// "<" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x30,			
	0x00,0x1C,			
	0x00,0x07,			
	0xC0,0x01,			
	0x70,0x00,			
	0x1C,0x00,			
	0x3C,0x00,			
	0xF0,0x00,			
	0xC0,0x03,			
	0x00,0x0F,			
	0x00,0x3C,			
	0x00,0x30,			
	0x00,0x00,			
	0x00,0x00},			
// "=" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xFE,0x7F,			
	0x00,0x00,			
	0x00,0x00,			
	0xFE,0x7F,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// ">" 
{	0x00,0x00,			
	0x00,0x00,			
	0x0C,0x00,			
	0x38,0x00,			
	0xE0,0x00,			
	0x80,0x03,			
	0x00,0x0E,			
	0x00,0x38,			
	0x00,0x3C,			
	0x00,0x0F,			
	0xC0,0x03,			
	0xF0,0x00,			
	0x3C,0x00,			
	0x0C,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "?" 
{	0x00,0x00,			
	0xF0,0x0F,			
	0x1C,0x3C,			
	0x0E,0x70,			
	0x1E,0x70,			
	0x08,0x38,			
	0x00,0x1E,			
	0x80,0x07,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x00,0x00,			
	0xC0,0x03,			
	0xC0,0x03,			
	0x00,0x00,			
	0x00,0x00},
	 
// "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_" 

// "@" 
{	0x00,0x00,			
	0x00,0x03,			
	0xF0,0x1F,			
	0x38,0x30,			
	0x1C,0x7F,			
	0xCE,0x7D,			
	0xEE,0xCC,			
	0x66,0xCC,			
	0x76,0x6E,			
	0x7E,0x66,			
	0xEE,0x3F,			
	0x1C,0x60,			
	0x38,0x30,			
	0xF0,0x1F,			
	0x00,0x00,			
	0x00,0x00},			
// "A" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x01,			
	0xC0,0x03,			
	0xC0,0x03,			
	0x60,0x07,			
	0x60,0x07,			
	0x30,0x0E,			
	0x30,0x0E,			
	0xF8,0x1F,			
	0x1C,0x1C,			
	0x0C,0x38,			
	0x0E,0x38,			
	0x1F,0xFC,			
	0x00,0x00,			
	0x00,0x00},			
// "B" 
{	0x00,0x00,			
	0x00,0x00,			
	0xFF,0x03,			
	0x1C,0x3E,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x3C,			
	0xFC,0x0F,			
	0x1C,0x3C,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x3C,			
	0xFF,0x0F,			
	0x00,0x00,			
	0x00,0x00},			
// "C" 
{	0x00,0x00,			
	0x00,0x00,			
	0x80,0x07,			
	0xF0,0x7F,			
	0x3C,0x70,			
	0x0E,0x60,			
	0x0E,0x00,			
	0x0E,0x00,			
	0x0E,0x00,			
	0x0E,0x00,			
	0x0E,0x60,			
	0x1C,0x70,			
	0x78,0x1E,			
	0xC0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "D" 
{	0x00,0x00,			
	0x00,0x00,			
	0xFF,0x01,			
	0xBC,0x0F,			
	0x1C,0x3C,			
	0x1C,0x78,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x78,			
	0x1C,0x3C,			
	0x1C,0x1F,			
	0xFF,0x01,			
	0x00,0x00,			
	0x00,0x00},			
// "E" 
{	0x00,0x00,			
	0x00,0x00,			
	0xFE,0x3F,			
	0x1C,0x3C,			
	0x1C,0x60,			
	0x1C,0x00,			
	0x1C,0x0C,			
	0xFC,0x0F,			
	0x1C,0x0C,			
	0x1C,0x08,			
	0x1C,0x00,			
	0x1C,0x60,			
	0x1C,0x78,			
	0xFE,0x3F,			
	0x00,0x00,			
	0x00,0x00},			
// "F" 
{	0x00,0x00,			
	0x00,0x00,			
	0xFE,0x3F,			
	0x1C,0x78,			
	0x1C,0xE0,			
	0x1C,0x00,			
	0x1C,0x08,			
	0xFC,0x0F,			
	0x1C,0x0C,			
	0x1C,0x08,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x7F,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "G" 
{	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x13,			
	0xF8,0x1F,			
	0x1C,0x38,			
	0x0E,0x30,			
	0x0E,0x00,			
	0x0E,0x00,			
	0x06,0xFE,			
	0x0E,0x38,			
	0x0E,0x38,			
	0x1C,0x38,			
	0x78,0x3C,			
	0xE0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "H" 
{	0x00,0x00,			
	0x00,0x00,			
	0x3F,0xFC,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0xFC,0x3F,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x3F,0xFC,			
	0x00,0x00,			
	0x00,0x00},			
// "I" 
{	0x00,0x00,			
	0x00,0x00,			
	0xF8,0x1F,			
	0xC0,0x03,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0xF8,0x1F,			
	0x00,0x00,			
	0x00,0x00},			
// "J" 
{	0x00,0x00,			
	0x00,0x00,			
	0xE0,0xFF,			
	0x00,0x0F,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x0E,0x07,			
	0x8E,0x03,			
	0xFC,0x00},			
// "K" 
{	0x00,0x00,			
	0x00,0x00,			
	0x7F,0x7E,			
	0x1C,0x3C,			
	0x1C,0x0E,			
	0x1C,0x03,			
	0xDC,0x01,			
	0xFC,0x01,			
	0xBC,0x03,			
	0x1C,0x07,			
	0x1C,0x0E,			
	0x1C,0x1C,			
	0x1C,0x38,			
	0x7F,0xFC,			
	0x00,0x00,			
	0x00,0x00},			
// "L" 
{	0x00,0x00,			
	0x00,0x00,			
	0x7E,0x00,			
	0x3C,0x00,			
	0x18,0x00,			
	0x18,0x00,			
	0x18,0x00,			
	0x18,0x00,			
	0x18,0x00,			
	0x18,0x00,			
	0x18,0x00,			
	0x18,0x60,			
	0x18,0x78,			
	0xFE,0x3F,			
	0x00,0x00,			
	0x00,0x00},			
// "M" 
{	0x00,0x00,			
	0x00,0x00,			
	0x0F,0xF8,			
	0x1E,0x78,			
	0x1C,0x3C,			
	0x3C,0x3C,			
	0x3C,0x36,			
	0x34,0x36,			
	0x74,0x33,			
	0x64,0x33,			
	0xE4,0x31,			
	0xE4,0x31,			
	0xC4,0x30,			
	0xCF,0xFC,			
	0x00,0x00,			
	0x00,0x00},			
// "N" 
{	0x00,0x00,			
	0x00,0x00,			
	0x1F,0xFC,			
	0x3C,0x30,			
	0x7C,0x20,			
	0xF4,0x20,			
	0xE4,0x21,			
	0xC4,0x23,			
	0x84,0x27,			
	0x04,0x2F,			
	0x04,0x3E,			
	0x04,0x3C,			
	0x0C,0x38,			
	0x3F,0x30,			
	0x00,0x00,			
	0x00,0x00},			
// "O" 
{	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xF8,0x1F,			
	0x1C,0x38,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x1C,0x38,			
	0x78,0x1E,			
	0xC0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "P" 
{	0x00,0x00,			
	0x00,0x00,			
	0xFE,0x07,			
	0x1C,0x3E,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x3C,			
	0xFC,0x07,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x7F,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "Q" 
{	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x03,			
	0xF8,0x1F,			
	0x1C,0x38,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x70,			
	0xFE,0x73,			
	0x3C,0x3F,			
	0x78,0x1E,			
	0xE0,0x7F,			
	0x00,0x3C,			
	0x00,0x00},			
// "R" 
{	0x00,0x00,			
	0x00,0x00,			
	0xFE,0x07,			
	0x3C,0x3E,			
	0x18,0x38,			
	0x18,0x30,			
	0x18,0x38,			
	0xF8,0x1F,			
	0x18,0x07,			
	0x18,0x07,			
	0x18,0x0E,			
	0x18,0x1C,			
	0x1C,0x38,			
	0x7E,0xF8,			
	0x00,0x00,			
	0x00,0x00},			
// "S" 
{	0x00,0x00,			
	0x00,0x00,			
	0xE0,0x13,			
	0xFC,0x1F,			
	0x0E,0x38,			
	0x0E,0x30,			
	0x3C,0x00,			
	0xF8,0x07,			
	0x80,0x1F,			
	0x00,0x3C,			
	0x06,0x30,			
	0x0E,0x30,			
	0x3C,0x3C,			
	0xE4,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "T" 
{	0x00,0x00,			
	0x00,0x00,			
	0xFC,0x3F,			
	0xFE,0x7F,			
	0x86,0x61,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0xC0,0x01,			
	0xF0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "U" 
{	0x00,0x00,			
	0x00,0x00,			
	0x3F,0xFC,			
	0x1C,0x30,			
	0x1C,0x30,			
	0x1C,0x30,			
	0x1C,0x30,			
	0x1C,0x30,			
	0x1C,0x30,			
	0x1C,0x30,			
	0x1C,0x30,			
	0x1C,0x30,			
	0x78,0x1E,			
	0xE0,0x03,			
	0x00,0x00,			
	0x00,0x00},			
// "V" 
{	0x00,0x00,			
	0x00,0x00,			
	0x3F,0x7C,			
	0x1E,0x78,			
	0x1C,0x18,			
	0x38,0x18,			
	0x38,0x0C,			
	0x70,0x0C,			
	0x70,0x06,			
	0xE0,0x06,			
	0xE0,0x03,			
	0xC0,0x01,			
	0xC0,0x01,			
	0x80,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "W" 
{	0x00,0x00,			
	0x00,0x00,			
	0xDF,0xF7,			
	0xCE,0x63,			
	0x8E,0x23,			
	0x8C,0x33,			
	0x9C,0x33,			
	0xDC,0x17,			
	0xD8,0x1F,			
	0x78,0x1F,			
	0x78,0x0E,			
	0x78,0x0E,			
	0x30,0x0E,			
	0x30,0x04,			
	0x00,0x00,			
	0x00,0x00},			
// "X" 
{	0x00,0x00,			
	0x00,0x00,			
	0x7E,0x7C,			
	0x3C,0x38,			
	0x38,0x0C,			
	0x70,0x06,			
	0xE0,0x07,			
	0xC0,0x03,			
	0xC0,0x03,			
	0xE0,0x07,			
	0x70,0x0E,			
	0x38,0x1C,			
	0x1C,0x38,			
	0x3E,0xFC,			
	0x00,0x00,			
	0x00,0x00},			
// "Y" 
{	0x00,0x00,			
	0x00,0x00,			
	0x7F,0x7C,			
	0x1C,0x38,			
	0x38,0x18,			
	0x78,0x0C,			
	0x70,0x06,			
	0xE0,0x06,			
	0xC0,0x03,			
	0xC0,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0xC0,0x01,			
	0xF0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "Z" 
{	0x00,0x00,			
	0x00,0x00,			
	0xF8,0x7F,			
	0x7C,0x38,			
	0x0E,0x1C,			
	0x00,0x0E,			
	0x00,0x07,			
	0x80,0x03,			
	0xC0,0x01,			
	0xE0,0x00,			
	0x78,0x00,			
	0x3C,0x60,			
	0x1E,0x38,			
	0xFE,0x3F,			
	0x00,0x00,			
	0x00,0x00},			
// "[" 
{	0x00,0x00,			
	0xC0,0x3F,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x00,			
	0xC0,0x3F,			
	0x00,0x00},			
// "\" 
{	0x00,0x00,			
	0x00,0x00,			
	0x3F,0x7C,			
	0x1E,0x38,			
	0x3C,0x18,			
	0x78,0x0C,			
	0x70,0x06,			
	0xFC,0x3F,			
	0xC0,0x03,			
	0xFC,0x3F,			
	0x80,0x01,			
	0x80,0x01,			
	0xC0,0x01,			
	0xF0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "]" 
{	0x00,0x00,			
	0xFC,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0x00,0x03,			
	0xFC,0x03,			
	0x00,0x00},			
// "^" 
{	0xC0,0x03,			
	0xF0,0x0F,			
	0x30,0x0C,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "_" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xFF,0xFF,			
	0x00,0x00}, 
// "`abcdefghijklmnopqrstuvwxyz{|}~" 
// "`" 
{	0x7C,0x00,			
	0xF8,0x01,			
	0x00,0x01,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00},			
// "a" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xF8,0x0F,			
	0x1C,0x1C,			
	0x1C,0x1C,			
	0xF0,0x1F,			
	0x3C,0x1C,			
	0x0E,0x1C,			
	0x1E,0x7F,			
	0xF8,0x39,			
	0x00,0x00,			
	0x00,0x00},			
// "b" 
{	0x00,0x00,			
	0x00,0x00,			
	0x1E,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0xDC,0x0F,			
	0x7C,0x3C,			
	0x3C,0x38,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x38,			
	0x7C,0x1C,			
	0xCC,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "c" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xE0,0x0F,			
	0x78,0x1C,			
	0x1C,0x1C,			
	0x0C,0x00,			
	0x0C,0x00,			
	0x1C,0x30,			
	0x78,0x1C,			
	0xE0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "d" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x3E,			
	0x00,0x38,			
	0x00,0x38,			
	0x00,0x38,			
	0xE0,0x3B,			
	0x78,0x3E,			
	0x1C,0x38,			
	0x0C,0x38,			
	0x0C,0x38,			
	0x1C,0x3C,			
	0x78,0x7F,			
	0xE0,0x1B,			
	0x00,0x00,			
	0x00,0x00},			
// "e" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xE0,0x0F,			
	0x38,0x3C,			
	0x1C,0x38,			
	0xFC,0x3F,			
	0x0C,0x00,			
	0x1C,0x30,			
	0x78,0x1E,			
	0xE0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "f" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x1E,			
	0xC0,0x7F,			
	0xE0,0x70,			
	0xE0,0x00,			
	0xFE,0x1F,			
	0xE0,0x00,			
	0xE0,0x00,			
	0xE0,0x00,			
	0xE0,0x00,			
	0xE0,0x00,			
	0xE0,0x00,			
	0xFC,0x0F,			
	0x00,0x00,			
	0x00,0x00},			
// "g" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xF0,0x7F,			
	0x38,0x7E,			
	0x1C,0x1C,			
	0x1C,0x1C,			
	0xF8,0x0F,			
	0x3C,0x00,			
	0xF8,0x3F,			
	0x0E,0x38,			
	0x1E,0x38,			
	0xF8,0x1F},			
// "h" 
{	0x00,0x00,			
	0x00,0x00,			
	0x1E,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0xDC,0x0F,			
	0xFC,0x1C,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x7E,0x7E,			
	0x00,0x00,			
	0x00,0x00},			
// "i" 
{	0x00,0x00,			
	0x00,0x00,			
	0x80,0x03,			
	0xC0,0x03,			
	0x00,0x00,			
	0x00,0x00,			
	0xF8,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0xC0,0x01,			
	0xF8,0x1F,			
	0x00,0x00,			
	0x00,0x00},			
// "j" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x1C,			
	0x00,0x1E,			
	0x00,0x00,			
	0x00,0x00,			
	0x80,0x1F,			
	0x00,0x1C,			
	0x00,0x1C,			
	0x00,0x1C,			
	0x00,0x1C,			
	0x00,0x1C,			
	0x00,0x1C,			
	0x00,0x1C,			
	0x1C,0x0E,			
	0xF8,0x03},			
// "k" 
{	0x00,0x00,			
	0x00,0x00,			
	0x1E,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x1C,0x00,			
	0x1C,0x3F,			
	0x1C,0x0E,			
	0x9C,0x03,			
	0xFC,0x03,			
	0x3C,0x07,			
	0x1C,0x0E,			
	0x1C,0x3C,			
	0x7E,0x7E,			
	0x00,0x00,			
	0x00,0x00},			
// "l" 
{	0x00,0x00,			
	0x00,0x00,			
	0xF8,0x01,			
	0xC0,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0xC0,0x01,			
	0xF8,0x1F,			
	0x00,0x00,			
	0x00,0x00},			
// "m" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xFF,0x3D,			
	0xDE,0x77,			
	0x8E,0x71,			
	0x8E,0x71,			
	0x8E,0x71,			
	0x8E,0x71,			
	0x8E,0x73,			
	0xDF,0xFF,			
	0x00,0x00,			
	0x00,0x00},			
// "n" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xDE,0x0F,			
	0x7C,0x1C,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x1C,0x38,			
	0x7E,0x7E,			
	0x00,0x00,			
	0x00,0x00},			
// "o" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xF0,0x07,			
	0x3C,0x1C,			
	0x0E,0x38,			
	0x0E,0x70,			
	0x0E,0x70,			
	0x0E,0x38,			
	0x38,0x1E,			
	0xE0,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "p" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xDE,0x0F,			
	0x7C,0x3C,			
	0x1C,0x78,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x1C,0x70,			
	0x7C,0x3C,			
	0xDC,0x0F,			
	0x1C,0x00,			
	0x7F,0x00},			
// "q" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xE0,0x17,			
	0x38,0x1E,			
	0x1C,0x18,			
	0x0E,0x18,			
	0x0E,0x18,			
	0x0C,0x1C,			
	0x3C,0x1F,			
	0xE0,0x19,			
	0x00,0x18,			
	0x00,0x7E},			
// "r" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x7E,0x7E,			
	0xF0,0x73,			
	0xF0,0x00,			
	0x70,0x00,			
	0x70,0x00,			
	0x70,0x00,			
	0x70,0x00,			
	0xFE,0x03,			
	0x00,0x00,			
	0x00,0x00},			
// "s" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xF0,0x1F,			
	0x38,0x3C,			
	0x38,0x30,			
	0xF8,0x07,			
	0x00,0x3F,			
	0x0C,0x38,			
	0x3C,0x3C,			
	0xEC,0x07,			
	0x00,0x00,			
	0x00,0x00},			
// "t" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xC0,0x00,			
	0xE0,0x00,			
	0xFC,0x1F,			
	0xE0,0x00,			
	0xE0,0x00,			
	0xE0,0x00,			
	0xE0,0x00,			
	0xE0,0x30,			
	0xE0,0x3D,			
	0x80,0x0F,			
	0x00,0x00,			
	0x00,0x00},			
// "u" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x1E,0x1E,			
	0x1C,0x18,			
	0x1C,0x18,			
	0x1C,0x18,			
	0x1C,0x18,			
	0x1C,0x1C,			
	0xFC,0x7F,			
	0xF0,0x19,			
	0x00,0x00,			
	0x00,0x00},			
// "v" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x7E,0x7C,			
	0x38,0x18,			
	0x38,0x0C,			
	0x70,0x0C,			
	0xE0,0x06,			
	0xE0,0x03,			
	0xC0,0x01,			
	0x80,0x01,			
	0x00,0x00,			
	0x00,0x00},			
// "w" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xFF,0xFB,			
	0x8E,0x71,			
	0x8C,0x33,			
	0xDC,0x1B,			
	0xD8,0x1F,			
	0x78,0x0E,			
	0x70,0x0E,			
	0x30,0x04,			
	0x00,0x00,			
	0x00,0x00},			
// "x" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xFC,0x3E,			
	0x70,0x0C,			
	0xE0,0x06,			
	0xC0,0x03,			
	0xC0,0x07,			
	0x70,0x0E,			
	0x38,0x3C,			
	0x3E,0x7E,			
	0x00,0x00,			
	0x00,0x00},			
// "y" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x7E,0x7E,			
	0x38,0x18,			
	0x70,0x0C,			
	0x70,0x0E,			
	0xE0,0x06,			
	0xC0,0x03,			
	0x80,0x01,			
	0x80,0x01,			
	0xF8,0x00,			
	0x78,0x00},			
// "z" 
{	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0xFC,0x1F,			
	0x1C,0x1E,			
	0x0C,0x07,			
	0xC0,0x03,			
	0xE0,0x00,			
	0x70,0x30,			
	0x3C,0x3C,			
	0xFC,0x1F,			
	0x00,0x00,			
	0x00,0x00},			
// "{" 
{	0x00,0x00,			
	0x00,0x3C,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0xC0,0x07,			
	0xC0,0x07,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x06,			
	0x00,0x0E,			
	0x00,0x3C,			
	0x00,0x00},			
// "|" 
{	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01,			
	0x80,0x01},			
// "}" 
{	0x00,0x00,			
	0x3C,0x00,			
	0x70,0x00,			
	0x60,0x00,			
	0x60,0x00,			
	0x60,0x00,			
	0x60,0x00,			
	0xE0,0x03,			
	0xE0,0x03,			
	0x60,0x00,			
	0x60,0x00,			
	0x60,0x00,			
	0x60,0x00,			
	0x70,0x00,			
	0x3C,0x00,			
	0x00,0x00},			
// "~" 
{	0xFC,0x00,			
	0xC6,0x63,			
	0x02,0x3F,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00,			
	0x00,0x00}			

};		
// "`abcdefghijklmnopqrstuvwxyz{|}~" 			