/*********************************************************************************************************
*
* File                : TouchPanel.h
* Hardware Environment: 
* Build Environment   : RealView MDK-ARM  Version: 4.20
* Version             : V1.0
* By                  : 
*
*                                  (c) Copyright 2005-2011, WaveShare
*                                       http://www.waveshare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#ifndef _TOUCHPANEL_H_
#define _TOUCHPANEL_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "config.h"

/* Private typedef -----------------------------------------------------------*/
typedef	struct POINT 
{
   uint16_t x;
   uint16_t y;
}Coordinate;

extern Coordinate *nPtr,display;

typedef struct Matrix 
{						
double 	ScaleX,  
   		ScaleY,
   		BiasX,     
            	BiasY;   

} Matrix ;

/* Private variables ---------------------------------------------------------*/
extern Coordinate ScreenSample[3];
extern Coordinate DisplaySample[3];
extern Matrix matrix ;
extern Coordinate  display ;
extern Coordinate *nPtr,nPoint;
/* Private define ------------------------------------------------------------*/

#define	CHX 	0xD0
#define	CHY 	0x90
#define	CHZ		0xB0

/* Private function prototypes -----------------------------------------------*/		

#define SPI_CS_L()		GPIO_ResetBits(Open_SPI_NSS_GPIO_PORT, Open_SPI_NSS_PIN)
#define SPI_CS_H()		GPIO_SetBits(Open_SPI_NSS_GPIO_PORT, Open_SPI_NSS_PIN)

#define SPI_CLK_L()		GPIO_ResetBits(Open_SPI_SCK_GPIO_PORT, Open_SPI_SCK_PIN)
#define SPI_CLK_H()		GPIO_SetBits(Open_SPI_SCK_GPIO_PORT, Open_SPI_SCK_PIN)
#define SPI_MOSI_L()		GPIO_ResetBits(Open_SPI_MOSI_GPIO_PORT, Open_SPI_MOSI_PIN)
#define SPI_MOSI_H()		GPIO_SetBits(Open_SPI_MOSI_GPIO_PORT, Open_SPI_MOSI_PIN)
#define SPI_MISO()		GPIO_ReadInputDataBit(Open_SPI_MISO_GPIO_PORT,Open_SPI_MISO_PIN)
#define SPI_IRQ()			GPIO_ReadInputDataBit(Open_TP_IRQ_PORT,Open_TP_IRQ_PIN)


void TP_Init(void);	
Coordinate *Read_XTP2046(void );
Coordinate *Read_Ads7846(void);
void TouchPanel_Calibrate(void);
void TouchPanel_NoCalibrate(void);
void DrawCross(uint16_t Xpos,uint16_t Ypos);
void TP_DrawPoint(uint16_t Xpos,uint16_t Ypos, u16 Color);
FunctionalState setCalibrationMatrix( Coordinate * displayPtr,Coordinate * screenPtr,Matrix * matrixPtr);
FunctionalState getDisplayPoint(Coordinate * displayPtr,Coordinate * screenPtr,Matrix * matrixPtr );
void SPI_LCD_Init(void) ;
void delay_ms(uint16_t ms)    ;
void TouchPoint( void );
void Touch_Read( void );
void Power_Down( void );
#endif

/*********************************************************************************************************
      END FILE
*********************************************************************************************************/


