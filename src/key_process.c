#include "stm32f4xx.h"
#include "extern.h"
#include "main.h"
#include "LCD.h"
#include "key_process.h"
#include "rgb_color_table.h"
#include "display.h"
#include "Protocol.h"

int	_nKey_Count = 0;

int	_nStartCursorPos,_nEndCursorPos,_nDecimalPos, _nCurrentPos;

void key_process(void)
{

	switch(System.nMenu  )
	{
		case	MENU_READY:
				KeyReadyProcess();
				break;
		case	MENU_SOUND_CHECK:
				KeySoundCheckProcess();
				break;
		case	MENU_SOUND_FAIL:
				KeySoundFailProcess();
				break;
		case	MENU_SOUND_OK:
				KeySoundOKProcess();
				break;
		case	MENU_MEASURE:
				KeyMeasureProcess();
				break;
		case	MENU_TOTAL:
				KeyTotalProcess();
				break;
	
		}
//	System.nButton = BUTTON_OPEN;

	System.OnRefresh = TRUE ;
}

void KeySoundCheckProcess( void )
{


}
void KeySoundFailProcess( void )
{

	switch(System.nButton)
		{

			case	BUTTON_FAIL:
					System.nLastMenu = System.nMenu;
					System.nMenu = MENU_READY;

					break;

			default:
					System.OnRefresh = FALSE;
					break;
					
		}	
}

void KeySoundOKProcess( void )
{

	switch(System.nButton)
		{

			case	BUTTON_OK:
					System.nLastMenu = System.nMenu;
					System.nMenu = MENU_MEASURE;

					break;

			default:
					System.OnRefresh = FALSE;
					break;
					
		}	
}
void KeyReadyProcess(void)
{

	switch(System.nButton)
		{

			case	BUTTON_TOTAL:
					System.nLastMenu = System.nMenu;
					System.nMenu = MENU_TOTAL;
					System.nTempValue = Motor.nTotal;

					break;

			case	BUTTON_START:
					System.nLastMenu = System.nMenu;
					System.nMenu = MENU_SOUND_CHECK;
					System.fDelta = 0.f;
					System.fOffset = 0.f;
					System.nDelay = 0;
//					System.nMenu = MENU_MEASURE;
					MotorTest =  MEASURE_READY;
					Motor.nCNT = 0;
					Motor.nErrorCNT = 0;	


					break;
			default:
					System.OnRefresh = FALSE;
					break;
					
		}	
	
}


void KeyMeasureProcess(void)
{

	switch(System.nButton)
		{


			case	BUTTON_START:
					System.nLastMenu = System.nMenu;
					System.nMenu = MENU_READY;
					//MotorTest = CYCLE_DONE;
					MotorTest = ALL_DONE;
					break;
			default:
					System.OnRefresh = FALSE;
					break;
					
		}	
	
}



void KeyTotalProcess(void)
{

	switch(System.nButton)
		{


			case 	BUTTON_ENTER:
					System.nLastMenu = System.nMenu;
					System.nMenu = MENU_READY;
					Motor.nTotal = System.nTempValue ;
					Motor.nCNT = 0;
					Motor.nErrorCNT = 0;					
					break;
			case 	BUTTON_UP:
					System.nTempValue += 10;
					if ( System.nTempValue > 1000 )
						System.nTempValue = 1000;
	
					break;

			case 	BUTTON_DOWN :
					System.nTempValue -= 10;
					if ( System.nTempValue < 10 )
						System.nTempValue = 10;

					break;
			default:

					break;

			
		}
	

}



