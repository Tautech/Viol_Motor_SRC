
#include "fonts.h"
#include <stdlib.h>
#include "display.h"
#include "LCD.h"
#include "rgb_color_table.h"
#include "key_process.h"
#include "extern.h"
#include "Protocol.h"
#include "main.h"
#include "TouchPanel.h"

uint16_t x_pos;

u16	Back_Color =RGB_BACKGROUND;// RGB_GAINSBORO;

int	_nLastMenuStatus ;

int Display_LCD(void)
{
	
	switch(System.nMenu)
		{
		case	MENU_READY:
				System.nDisplayMode = MENU_READY;
				if ( System.nDisplayMode != System.nLastDisplayMode ){
					Display_Measure_Frame();
					
					}
				if ( System.OnRefresh ){
					Display_Ready();
//					controller.OnRefresh = 0;
					}
				System.nLastDisplayMode = System.nDisplayMode;
				break;
		case	MENU_SOUND_CHECK:
				System.nDisplayMode = MENU_SOUND_CHECK;
				if ( System.nDisplayMode != System.nLastDisplayMode ){
					LCD_Clear( RGB_CYAN );
					
					}
				if ( System.OnRefresh ){
					Display_SoundCheck_Frame();
//					Display_SoundCheck();
//					controller.OnRefresh = 0;
					}
				System.nLastDisplayMode = System.nDisplayMode;
				break;
		case	MENU_SOUND_FAIL :
				System.nDisplayMode = MENU_SOUND_FAIL;

				if ( System.OnRefresh ){
					Display_Sound_Error( FALSE );
//					Display_SoundCheck();
//					controller.OnRefresh = 0;
					}
				System.nLastDisplayMode = System.nDisplayMode;
				break;
#if 1
              case	MENU_SOUND_OK :
				System.nDisplayMode = MENU_SOUND_OK;

				if ( System.OnRefresh ){
					Display_Sound_Error( TRUE );
//					Display_SoundCheck();
//					controller.OnRefresh = 0;
					}
				System.nLastDisplayMode = System.nDisplayMode;
				break;
#endif				
		case	MENU_MEASURE:
//				printf(" TFT_Disp_Meausre !! \r\n");
				System.nDisplayMode = MENU_MEASURE;
				if ( System.nDisplayMode != System.nLastDisplayMode ){
					Display_Measure_Frame();
					
					}
				if ( System.OnRefresh ){
					Display_Ready();
//					controller.OnRefresh = 0;
					}
				System.nLastDisplayMode = System.nDisplayMode;
				return 0;
				break;
				
		case	MENU_TOTAL:
				System.nDisplayMode = MENU_TOTAL ;
				if ( System.nDisplayMode != System.nLastDisplayMode ) {
					Display_Total_Button();
					
					}				

				if ( System.OnRefresh  ){
					Display_Total();
					System.OnRefresh = 0;
					}
				System.nLastDisplayMode = System.nDisplayMode;
				return 0;

				break;		

		default:
//				controller.nDisplayMode =  (unsigned char)(controller.MenuCurrentStatus  & 0x00F0 );
				break;
		}
	
	System.OnRefresh = 0;
//	System.nLastMenu = System.nMenu;
	return 0;

}

void Display_Measure( void )
{


}
void Display_SoundCheck_Frame( void )
{
	char	strBuff[24];
	u16	xPos, yPos;//, xOffset = 60, yOffset = 28;

	if ( !System.nSoundOK){
		strcpy(strBuff,"Sound Error!!");
		xPos = 300;yPos = 240;
		LCD_String_Write(xPos,yPos,strBuff,16,32,RGB_BLACK,RGB_RED,NORMAL);	

		}

}

u8	nLastdB[1024];
void Display_SoundCheck( u8 ADDR )
{
	       u16       i,xPos, yPos;
		u16		dist, color;   
		char		strBuff[24];
		u8		*pdBData, *pdBLastData;
	        
		pdBData  = (void*)&dBData;
		pdBLastData = ( void*)&nLastdB;
		
		if ( ADDR ){
			pdBData += ADC_FFT_SIZE /2;
			pdBLastData += ADC_FFT_SIZE /4;
			}
		
		xPos = TEXT_START;yPos = 240 + 240*ADDR;
		sprintf(strBuff,"Now => %03d: %06.2f",System.nIndexMax[ADDR], System.fdBMax[ADDR]);
		LCD_String_Write(xPos, yPos -40,strBuff,10,16,RGB_BLACK,RGB_CYAN,NORMAL);

		for ( i = 0;i<ADC_FFT_SIZE/4-1 ;i++){
			LCD_DrawGraph(i ,yPos -(u16)*(pdBLastData + i ) , yPos -(u16)*(pdBLastData + i + 1 ),RGB_CYAN);
			}
		for ( i = 0;i<ADC_FFT_SIZE / 4  ;i++)
			*(pdBLastData + i ) = *(pdBData + i );

		for ( i = 0;i<ADC_FFT_SIZE/4-1;i++){
			LCD_DrawGraph(i ,yPos -(u16) *(pdBData + i ) , yPos -(u16)*(pdBData + i + 1 ),RGB_BLACK);
			}

		xPos = 0;yPos = yPos -REFERENCE_LEVEL;dist = ADC_FFT_SIZE/4 ;color = RGB_RED;
		LCD_Draw_xline( xPos, yPos, dist, color);
		LCD_Draw_xline( xPos, yPos+1, dist, color);
#if 0
		if ( Motor.nCNT == Motor.nTotal ){
			Calcu_STD_Dev();
			sprintf(strBuff,"STD Dev=%4.2f: %4.2f",Motor.fSTDDEV[0], Motor.fSTDDEV[1]);
			LCD_String_Write(xPos, yPos -160,strBuff,12,24,RGB_BLACK,RGB_CYAN,NORMAL);
			}
#endif
}


void Display_Window( void )
{

	
#ifndef TFT_43
        u16	xPos, yPos, xSize, ySize, Color;
	xPos = 60;yPos = 40;
	xSize = 120;ySize = 60;
	Color = RGB_YELLOW;
	LCD_Color( xPos, yPos, xSize, ySize, Color );

	yPos = 180;
	LCD_Color( xPos, yPos, xSize, ySize, Color );

	xPos = 300;
	LCD_Color( xPos, yPos, xSize, ySize, Color );
#else
	Display_Measure_Frame();

#endif

}

void Display_Measure_Frame(void)
{
	u16	xPos, yPos, xSize=BOX_XSIZE, ySize=BOX_YSIZE;

	LCD_Clear(RGB_BLUE);	

	xPos = nTotal_X;yPos = nTotal_Y;
	LCD_Show_Image( xPos, yPos , xSize, ySize, YellowButton, NORMAL);

	xPos = nCount_X;yPos = nCount_Y;
//	LCD_Color( xPos, yPos, xSize, ySize, Color );
	LCD_Show_Image( xPos, yPos , xSize, ySize, YellowButton, NORMAL);

	xPos = nFail_X;yPos = nFail_Y;
//	LCD_Color( xPos, yPos, xSize, ySize, Color );
	LCD_Show_Image( xPos, yPos , xSize, ySize, YellowButton, NORMAL);

	xPos = nStart_X;yPos = nStart_Y;xSize = 90;ySize = 90;
//	LCD_Color( xPos, yPos, xSize, ySize, Color );
	if ( System.nMenu == MENU_MEASURE )
		LCD_Show_Image( xPos, yPos , xSize, ySize, StopButton, TRANSPARENT );
	else
		LCD_Show_Image( xPos, yPos , xSize, ySize, StartButton, TRANSPARENT );
}

void	Display_Ready(void)
{
	char	strBuff[24];
	u16	xPos, yPos, xOffset = 60, yOffset = 28;
	
	sprintf(strBuff,"%4d",Motor.nTotal );
	xPos = nTotal_X+xOffset;yPos = nTotal_Y +yOffset;
	LCD_String_Write(xPos,yPos,strBuff,16,32,RGB_BLACK,RGB_YELLOW,NORMAL);	

	sprintf(strBuff,"%4d",Motor.nCNT );
	xPos = nCount_X+xOffset;yPos = nCount_Y +yOffset;
	LCD_String_Write(xPos,yPos,strBuff,16,32,RGB_BLACK,RGB_YELLOW,NORMAL);	

	sprintf(strBuff,"%4d",Motor.nErrorCNT );
	xPos = nFail_X+xOffset;yPos = nFail_Y +yOffset;
	LCD_String_Write(xPos,yPos,strBuff,16,32,RGB_RED,RGB_YELLOW,NORMAL);


}

void Display_Total(void)
{
	char	strBuff[24];
	u16	xPos, yPos, xOffset = 60, yOffset = 28;
	
	sprintf(strBuff,"%4d",System.nTempValue);
	xPos = nTotal_X+xOffset;yPos = nTotal_Y +yOffset;
	LCD_String_Write(xPos,yPos,strBuff,16,32,RGB_BLUE,RGB_YELLOW,NORMAL);		

}

void Display_Total_Button(void )
{
	u16	 xPos, yPos;

	xPos = nStart_X;yPos = nStart_Y;
	LCD_Color( xPos, yPos, 90, 90, RGB_BLUE );
	xPos = BUTTON_UP_X; yPos = BUTTON_UP_Y;
	LCD_Show_Image( xPos, yPos , BUTTON_XSIZE, BUTTON_YSIZE, UpArrow, TRANSPARENT);
	xPos = BUTTON_DOWN_X; yPos = BUTTON_DOWN_Y;
	LCD_Show_Image( xPos, yPos , BUTTON_XSIZE, BUTTON_YSIZE, DownArrow, TRANSPARENT);	
//	xPos = BUTTON_MENU_X; yPos = BUTTON_MENU_Y;
//	LCD_Show_Image( xPos, yPos , BUTTON_XSIZE, BUTTON_YSIZE, MenuIcon, TRANSPARENT);		
	xPos = BUTTON_ENTER_X; yPos = BUTTON_ENTER_Y;
	LCD_Show_Image( xPos, yPos , BUTTON_SIZE, BUTTON_SIZE, Enter3Icon, TRANSPARENT);	


}


void Display_Button( void )
{
	char		strBuff[32];
	u16		xPos =100, yPos;
	
	switch( System.nMenu ){
		case	MENU_READY:
				strcpy( strBuff," Ready  ");
						break;
		case	MENU_SOUND_CHECK:	
				strcpy( strBuff," Sound  ");
						break;
		case	MENU_MEASURE:	
				strcpy( strBuff," Measure");
				break;						

		default:	strcpy( strBuff," Total   ");
				break;
		}
	yPos = 150;
	LCD_String_Write( xPos, yPos, strBuff , 12 , 24, RGB_BLACK , RGB_YELLOW, NORMAL );
	
	switch( System.nButton ){
		case	BUTTON_TOTAL:	
				strcpy( strBuff," Total ");
				break;
		case	BUTTON_UP:	
				strcpy( strBuff," Up    ");
				break;
		case	BUTTON_DOWN:	
				strcpy( strBuff," Down  ");
				break;						
		case	BUTTON_ENTER:	
				strcpy( strBuff," Enter ");
				break;
		case	BUTTON_START:	
				strcpy( strBuff," Start ");
				break;
		default:			
				strcpy( strBuff," Blank ");
				break;
		}
	yPos = 180;
	LCD_String_Write( xPos, yPos, strBuff , 12 , 24, RGB_BLACK , RGB_YELLOW, NORMAL );

	switch( System.nLastStatus ){
		case	BUTTON_PRESSED:
				strcpy( strBuff," Pressed ");
				break;

		case	BUTTON_OPEN:	
				strcpy( strBuff," Open    ");
				break;

		}
	yPos = 210;
	LCD_String_Write( xPos, yPos, strBuff , 12 , 24, RGB_BLACK , RGB_YELLOW, NORMAL );
	
	switch( System.nStatus ){
		case	BUTTON_PRESSED:
				strcpy( strBuff," Pressed ");
				break;

		case	BUTTON_OPEN:	
				strcpy( strBuff," Open    ");
				break;
					

		}

	xPos = 200;yPos = 210;
	LCD_String_Write( xPos, yPos, strBuff , 12 , 24, RGB_BLACK , RGB_YELLOW, NORMAL );

	DisplayTouchPoint();
	
}

void DisplayTouchPoint( void )
{	
	u16		xPos, yPos;
	char 	strBuff[32];
	
 	sprintf(strBuff,"X Pos:%03d, Y Pos:%03d",display.x,display.y  );	   
	xPos = 400;yPos = 200;
	LCD_String_Write(xPos,yPos,strBuff,12,24,RGB_BLACK,RGB_YELLOW,NORMAL);	
}

void Display_Sound_Error( u8 nResult )
{
	u16	i,xPos, yPos;
	char strBuff[32], strValue1[32], strValue2[32];

	if ( nResult ){
		for ( i = CHECK_NUM - 3;i<CHECK_NUM;i++ ){
			Back_Color = RGB_GREEN;		
			sprintf(strValue1,"%04d :%6.2f", Motor.nCheckIndex[0][i], Motor.fCheckValue[0][i] );
			sprintf(strValue2,"%04d :%6.2f", Motor.nCheckIndex[1][i], Motor.fCheckValue[1][i] );
			xPos = TEXT_START;yPos =20*i - 10;
			LCD_String_Write(xPos, yPos ,strValue1,10,16,RGB_BLACK, Back_Color , NORMAL);	
			LCD_String_Write(xPos, yPos+240 ,strValue2,10,16,RGB_BLACK, Back_Color , NORMAL);	


			}
		sprintf(strValue1,"%6.2f :%6.2f", Motor.fAvgIndex[0], Motor.fAvgValue[0] );
		sprintf(strValue2,"%6.2f :%6.2f", Motor.fAvgIndex[1], Motor.fAvgValue[1] );
		xPos = TEXT_START;yPos = 120;
		LCD_String_Write(xPos, yPos ,strValue1,10,16,RGB_BLACK, Back_Color , NORMAL);	
		LCD_String_Write(xPos, yPos+240 ,strValue2,10,16,RGB_BLACK, Back_Color , NORMAL);	
		
		sprintf( strBuff,"Freq Offset=%6.1fHz", System.fOffset );
		xPos = TEXT_START;yPos = 200;
		LCD_String_Write(xPos, yPos ,strBuff ,12,24,RGB_BLACK, RGB_GREENYELLOW, NORMAL);	
		

		strcpy( strBuff, "Sound OK!!");
		xPos = FAIL_ICON_X; yPos = FAIL_ICON_Y;
		LCD_Show_Image( xPos, yPos , BUTTON_SIZE, BUTTON_SIZE, SoundOK_Green, TRANSPARENT);		
		}
	else{

		for ( i = CHECK_NUM - 3;i<CHECK_NUM;i++ ){
			Back_Color = RGB_RED;		
			sprintf(strValue1,"%04d :%6.2f", Motor.nCheckIndex[0][i], Motor.fCheckValue[0][i] );
			sprintf(strValue2,"%04d :%6.2f", Motor.nCheckIndex[1][i], Motor.fCheckValue[1][i] );
			xPos = TEXT_START;yPos =  20*i -10 ;
			LCD_String_Write(xPos, yPos ,strValue1,10,16,RGB_BLACK, Back_Color , NORMAL);	
			LCD_String_Write(xPos, yPos+240 ,strValue2,10,16,RGB_BLACK, Back_Color , NORMAL);	


			}
		sprintf(strValue1,"%6.2f :%6.2f", Motor.fAvgIndex[0], Motor.fAvgValue[0] );
		sprintf(strValue2,"%6.2f :%6.2f", Motor.fAvgIndex[1], Motor.fAvgIndex[1] );
		xPos = TEXT_START;yPos = 120;
		LCD_String_Write(xPos, yPos ,strValue1,10,16,RGB_BLACK, Back_Color , NORMAL);	
		LCD_String_Write(xPos, yPos+240 ,strValue2,10,16,RGB_BLACK, Back_Color , NORMAL);	
		


		strcpy( strBuff, "Sound Error!!");
		xPos = FAIL_ICON_X; yPos = FAIL_ICON_Y;
		LCD_Show_Image( xPos, yPos , BUTTON_SIZE, BUTTON_SIZE, SoundError , TRANSPARENT);			
		}
	xPos = 100;yPos = 240;
	LCD_String_Write(xPos,yPos,strBuff,12,24,RGB_BLACK, Back_Color , NORMAL);	
	delay_ms( 100 );
}

void Display_Sound_OK( u8 nResult )
{
	u16	i,xPos, yPos;
	char strBuff[32], strValue1[32], strValue2[32];

	if ( nResult ){
		return;
		}
	else{

		for ( i = CHECK_NUM -3;i<CHECK_NUM;i++ ){
			Back_Color = RGB_RED;		
			sprintf(strValue1,"%04d :%6.2f", Motor.nCheckIndex[0][i], Motor.fCheckValue[0][i] );
			sprintf(strValue2,"%04d :%6.2f", Motor.nCheckIndex[1][i], Motor.fCheckValue[1][i] );
			xPos = 500;yPos = 30+ 20*i;
			LCD_String_Write(xPos, yPos ,strValue1,10,16,RGB_BLACK, Back_Color , NORMAL);	
			LCD_String_Write(xPos, yPos+240 ,strValue2,10,16,RGB_BLACK, Back_Color , NORMAL);	


			}
		sprintf(strValue1,"%6.2f :%6.2f", Motor.fAvgIndex[0], Motor.fAvgValue[0] );
		sprintf(strValue2,"%6.2f :%6.2f", Motor.fAvgIndex[1], Motor.fAvgIndex[1] );
		xPos = 500;yPos = 120;
		LCD_String_Write(xPos, yPos ,strValue1,10,16,RGB_BLACK, Back_Color , NORMAL);	
		LCD_String_Write(xPos, yPos+240 ,strValue2,10,16,RGB_BLACK, Back_Color , NORMAL);	
		


		strcpy( strBuff, "Sound Error!!");
		xPos = FAIL_ICON_X; yPos = FAIL_ICON_Y;
		LCD_Show_Image( xPos, yPos , BUTTON_SIZE, BUTTON_SIZE, SoundError , TRANSPARENT);			
		}
	xPos = 100;yPos = 240;
	LCD_String_Write(xPos,yPos,strBuff,12,24,RGB_BLACK, Back_Color , NORMAL);	
	delay_ms( 100 );
}

