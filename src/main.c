/**
  ******************************************************************************
  * @file    main.c 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    18-April-2011
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************  
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "KEY&LED.h"
#include "main.h"
#include "LCD.h"
#include "Extern.h"
#include "Display.h"
#include "rgb_color_table.h"
#include "Protocol.h"
#include "math.h"
#include "Arm_math.h" 
#include "arm_const_structs.h"
#include "TouchPanel.h"
#include "stdio.h"
#include "key_process.h"

#define SYSTEMTICK_PERIOD_MS 1

__IO uint32_t LocalTime_t = 0;
__IO uint32_t TimingDelay;

__IO uint16_t	ADC_Converted[ ADC_BUFFER_SIZE];
Flo32  ADC_Data[ADC_FFT_SIZE/2];
u8	  dBData[ADC_BUFFER_SIZE/2];
#define ADC_CDR_ADDRESS    ((uint32_t)0x40012308)

int ADC_SUM[2] , ADC_AVG;
TSystem  System;
TMotorParam Motor;
TMeasureStep  MotorTest;

u8  nLCD_Mode[7]={ 0x04, 0x00, 0x03, 0x1F, 0x01, 0xDF, 0x1F};
Coordinate *nPtr,nPoint;
u32 nTimeCNT=0;
u32	nStepCNT = 0;
u8	nNextStep = FALSE;
_Bool ADC_Ready = FALSE, nRefresh = FALSE;
u16	nCNT = 0;
#define TEST_NO




int	x_Pos[1], y_Pos[1];
main(void)
{
//	u8	nCheck;

//	RCC_ClocksTypeDef SystemClock;
	nPtr = (void*)&nPoint;
	SysTick_Configuration();
	TP_Init();	

	GPIO_Config();
//	EXTI_Config();
	LCD_Initializtion();
	
 	TouchPanel_Calibrate();
//	TouchPanel_NoCalibrate();
	ADC_Single_Config_ADC1();
//	LCD_TEST();
	Init_Parameter();
	Display_Window();
	Display_Ready();
	nPtr=Read_XTP2046();

	Motor.nCNT = 0;	

	while (1)
	{
	if (Key_Check() )
		Init_Parameter();

	nCNT = 50;
	do
	   	{
		    	nPtr=Read_XTP2046();
			nCNT--;
		   	}while( nPtr == (void*)0 && nCNT );
	
		if (getDisplayPoint(&display,nPtr, &matrix )){
			Check_Touch();
			}
		else{
			System.nLastStatus = System.nStatus;
			System.nStatus	= BUTTON_OPEN;
			}
		if( System.nButton ){
			key_process();
			System.nButton = BUTTON_BLANK;
			}



		if ( System.OnRefresh ){
			Display_LCD();
			 System.OnRefresh  = FALSE;
			}

		}
}

void Init_Parameter( void )
{

	System.fFREQ = 0.40;// 0.39
	System.fDelta = 0.f;
	System.nDelay = 0;
	System.nMenu = MENU_READY;
	System.nLastDisplayMode = MENU_READY;
	System.OnRefresh = TRUE;
	System.nSoundOK = TRUE;	
	Motor.nCNT	= 0;
	Motor.nTotal = 10;
	Motor.nErrorCNT	= 0;
	
	Motor.nMaxIndex[0]  = 1;
	Motor.nMaxIndex[1]  = 1;
	Motor.nMinIndex[0]  = 511;
	Motor.nMinIndex[1]  = 511;

	MotorTest = ALL_DONE;
	nNextStep = FALSE;
}

u8 Check_Touch( void )
{	u8	nCheck = 0;
	char	strBuff[32];
	switch( System.nMenu ){
		case	MENU_READY:
				if ( display.x > nTotal_X && display.x < nTotal_X+BOX_XSIZE && display.y > nTotal_Y && display.y < nTotal_Y + BOX_YSIZE){
					if (System.nButton != BUTTON_TOTAL ){
						System.nTempButton 	= BUTTON_TOTAL;
						System.nLastStatus 	= System.nStatus;
						System.nStatus		= BUTTON_PRESSED; 
						if ( System.nLastStatus  == BUTTON_OPEN  )
							System.nButton = System.nTempButton;	
						strcpy( strBuff,"Total  Pressed      ");
						}
					}
				else if (display.x > nStart_X  && display.x < nStart_X+90 && display.y > nStart_Y && display.y < nStart_Y + 90){
					if (System.nButton != BUTTON_START){
						System.nTempButton 	= BUTTON_START;
						System.nLastStatus 	= System.nStatus;
						System.nStatus		= BUTTON_PRESSED; 
						if ( System.nLastStatus  == BUTTON_OPEN  )
							System.nButton = System.nTempButton;	
						strcpy( strBuff,"Start Button Pressed");
						}
					}
				else{
					System.nTempButton = BUTTON_BLANK;
					System.nLastStatus 	= System.nStatus;
					System.nStatus		= BUTTON_PRESSED; 
					if ( System.nLastStatus  == BUTTON_OPEN )
						System.nButton = System.nTempButton;	
					strcpy( strBuff,"Blank Button Pressed");

					}
				break;
				
		case	MENU_MEASURE:
				if (display.x > nStart_X  && display.x < nStart_X+90 && display.y > nStart_Y && display.y < nStart_Y + 90){
					if (System.nButton != BUTTON_START){
						System.nTempButton 	= BUTTON_START;
						System.nLastStatus 	= System.nStatus;
						System.nStatus		= BUTTON_PRESSED; 
						if ( System.nLastStatus  == BUTTON_OPEN )
							System.nButton = System.nTempButton;	
						strcpy( strBuff,"Stop  Button Pressed");
						}
					}
				else{
					System.nTempButton 	= BUTTON_BLANK;
					System.nLastStatus 	= System.nStatus;
					System.nStatus		= BUTTON_PRESSED; 
					if ( System.nLastStatus  == BUTTON_OPEN  )
						System.nButton = System.nTempButton;	
					strcpy( strBuff,"Blank Button Pressed");

					}				
				break;
		case	MENU_SOUND_FAIL:
				if (display.x > FAIL_ICON_X  && display.x < FAIL_ICON_X+BUTTON_XSIZE && display.y > FAIL_ICON_Y && display.y < FAIL_ICON_Y + BUTTON_YSIZE){
					if (System.nButton != BUTTON_FAIL){
						System.nTempButton 	= BUTTON_FAIL;
						System.nLastStatus 	= System.nStatus;
						System.nStatus		= BUTTON_PRESSED; 
						if ( System.nLastStatus  == BUTTON_OPEN )
							System.nButton = System.nTempButton;	
						strcpy( strBuff,"Stop  Button Pressed");
						}
					}
				else{
					System.nTempButton 	= BUTTON_BLANK;
					System.nLastStatus 	= System.nStatus;
					System.nStatus		= BUTTON_PRESSED; 
					if ( System.nLastStatus  == BUTTON_OPEN  )
						System.nButton = System.nTempButton;	
					strcpy( strBuff,"Sound Button Pressed");

					}

				break;
#if 1
		case	MENU_SOUND_OK:
				if (display.x > FAIL_ICON_X  && display.x < FAIL_ICON_X+BUTTON_XSIZE && display.y > FAIL_ICON_Y && display.y < FAIL_ICON_Y + BUTTON_YSIZE){
					if (System.nButton != BUTTON_OK){
						System.nTempButton 	= BUTTON_OK;
						System.nLastStatus 	= System.nStatus;
						System.nStatus		= BUTTON_PRESSED; 
						if ( System.nLastStatus  == BUTTON_OPEN )
							System.nButton = System.nTempButton;	
						strcpy( strBuff,"Sound  Button Pressed");
						}
					}
				else{
					System.nTempButton 	= BUTTON_BLANK;
					System.nLastStatus 	= System.nStatus;
					System.nStatus		= BUTTON_PRESSED; 
					if ( System.nLastStatus  == BUTTON_OPEN  )
						System.nButton = System.nTempButton;	
					strcpy( strBuff,"Blank Button Pressed");

					}

				break;
#endif				
		case	MENU_TOTAL:
				if ( display.x > BUTTON_UP_X  && display.x < BUTTON_UP_X+BUTTON_XSIZE && display.y > BUTTON_UP_Y && display.y < BUTTON_UP_Y + BUTTON_YSIZE){
					if (System.nButton != BUTTON_UP ){
						System.nTempButton 	= BUTTON_UP;
						System.nLastStatus 	= System.nStatus;
						System.nStatus		= BUTTON_PRESSED; 
						if ( System.nLastStatus  == BUTTON_OPEN )
							System.nButton = System.nTempButton;	
						strcpy( strBuff,"Up Button Pressed   ");
						}
					}
				else if ( display.x > BUTTON_DOWN_X  && display.x < BUTTON_DOWN_X+BUTTON_XSIZE && display.y > BUTTON_DOWN_Y && display.y < BUTTON_DOWN_Y + BUTTON_YSIZE){
					if (System.nButton != BUTTON_DOWN ){
						System.nTempButton 	= BUTTON_DOWN;
						System.nLastStatus 	= System.nStatus;
						System.nStatus		= BUTTON_PRESSED; 
						if ( System.nLastStatus  == BUTTON_OPEN )
							System.nButton = System.nTempButton;	
						strcpy( strBuff,"Down Button Pressed ");
						}
					}

				else if (display.x > BUTTON_ENTER_X&& display.x < BUTTON_ENTER_X+BUTTON_XSIZE && display.y > BUTTON_ENTER_Y && display.y < BUTTON_ENTER_Y+ BUTTON_YSIZE ){
					if (System.nButton != BUTTON_ENTER){
						System.nTempButton 	= BUTTON_ENTER;
						System.nLastStatus 	= System.nStatus;
						System.nStatus		= BUTTON_PRESSED; 
						if ( System.nLastStatus  == BUTTON_OPEN )
							System.nButton = System.nTempButton;	
						strcpy( strBuff,"Enter Button Pressed");
						}
					}
				else{
					System.nTempButton 	= BUTTON_BLANK;
					System.nLastStatus 	= System.nStatus;
					System.nStatus		= BUTTON_PRESSED; 
					if ( System.nLastStatus  == BUTTON_OPEN )
						System.nButton = System.nTempButton;	
					strcpy( strBuff,"Blank Button Pressed");

					}
				break;
				}
	
	return nCheck;
}		

void GPIO_Config(void)
{

	GPIO_InitTypeDef      GPIO_InitStructure;
	RCC_AHB1PeriphClockCmd(USER_BUTTON_GPIO_CLK |LED_GPIO_CLK |START_GPIO_CLK, ENABLE);
 // 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  	GPIO_InitStructure.GPIO_Pin 	= USER_BUTTON_PIN ;
  	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType 	= GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd 	= GPIO_PuPd_NOPULL;	//GPIO_PuPd_UP;
  	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_25MHz;
  	GPIO_Init(USER_BUTTON_GPIO_PORT, &GPIO_InitStructure);

  	GPIO_InitStructure.GPIO_Pin 	= LED_PIN ;
  	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType 	= GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd 	= GPIO_PuPd_UP;	//GPIO_PuPd_UP;
  	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_25MHz;
  	GPIO_Init(LED_GPIO_PORT, &GPIO_InitStructure);         

  	GPIO_InitStructure.GPIO_Pin 	= START_PIN ;
  	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType 	= GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd 	= GPIO_PuPd_UP;	//GPIO_PuPd_UP;
  	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_25MHz;
  	GPIO_Init(START_GPIO_PORT, &GPIO_InitStructure);    	
	START_OFF;
}

void ADC_Single_Config_ADC1(void)
{
	ADC_InitTypeDef       ADC_InitStructure;
  	ADC_CommonInitTypeDef 	ADC_CommonInitStructure;
  	DMA_InitTypeDef       DMA_InitStructure;
  	GPIO_InitTypeDef      GPIO_InitStructure;

  	/* Enable ADCx, DMA and GPIO clocks ****************************************/ 
  	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2 |RCC_AHB1Periph_PORT, ENABLE);
  	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 |RCC_APB2Periph_ADC2 |RCC_APB2Periph_ADC3, ENABLE);
  	/* Configure ADC1 Channel0 pin as analog input ******************************/
  	GPIO_InitStructure.GPIO_Pin = ADC_Pin;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
  	GPIO_Init(ADC_PORT, &GPIO_InitStructure);

  	/* DMA2 Stream0 channel2 configuration **************************************/
  	DMA_InitStructure.DMA_Channel = DMA_Channel_0;  
  	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&ADC1->DR;	// (uint32_t)ADC_CDR_ADDRESS; //	//
  	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&ADC_Converted;
  	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
  	DMA_InitStructure.DMA_BufferSize = ADC_BUFFER_SIZE;
  	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
  	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
  	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
  	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
  	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
  	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
  	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         
  	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
  	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
  	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
  	DMA_Init(DMA2_Stream0, &DMA_InitStructure);

	DMA_Cmd(DMA2_Stream0, ENABLE);



  	/* ADC Common Init **********************************************************/
  	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;//ADC_Mode_Independent; ADC_DualMode_Interl ADC_DualMode_RegSimult
  	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div8;		// Div4 : 200kSPS @ 24MHz Main Clock
  	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_1;	
  	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
  	ADC_CommonInit(&ADC_CommonInitStructure);

  	/* ADC Init ****************************************************************/
  	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
 	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
  	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
 // 	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
  	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  	ADC_InitStructure.ADC_NbrOfConversion = 1;
  	ADC_Init(ADC1, &ADC_InitStructure);

  	/* ADC1 regular channel7 configuration **************************************/
  	ADC_RegularChannelConfig(ADC1, ADC_CHANNEL, 1, ADC_SampleTime_480Cycles);
	ADC_DMACmd(ADC1, ENABLE);	
	
 // 	ADC_Init(ADC2, &ADC_InitStructure);
  //	ADC_RegularChannelConfig(ADC2, ADC_CHANNEL, 1, ADC_SampleTime_480Cycles);
 //	ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 1, ADC_SampleTime_3Cycles);
  	/* Enable DMA request after last transfer (Single-ADC mode) */
  	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);
 // 	ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);	// DISABLE, ENABLE	- default:ENABLE
 
  	/* Enable ADC1 DMA */
 // 	

  	/* Enable ADC1 */
  	ADC_Cmd(ADC1, ENABLE);
 // 	ADC_Cmd(ADC2, ENABLE);
  	ADC_SoftwareStartConv(ADC1);


	
}


// 신호 처리 순서
// 1. Mixer Down * exp( -j*2*pi*FREQ / 1000 )
// 2. Filter FIR16
// 3. Decimation 16

//Flo32	fReal[ADC_BUFFER_SIZE];//
//Flo32	fImag[ADC_BUFFER_SIZE];

u8 ADC_INPUT( u8 ADDR )
{

	int 		i,j, n;//, nDeci;
	Flo32	fReal[ADC_BUFFER_SIZE],fImag[ADC_BUFFER_SIZE];
	u16 		*pADC_BASE;
	Flo32	 *pData;
	Flo32	re,im;

#if 0
	u8		*pdBData;
	pdBData = (void*)&dBData;
#endif

	pADC_BASE = (void*)&ADC_Converted;
	pData= ( void*)&ADC_Data;
//	if ( ADDR )
//		pData +=  ADC_FFT_SIZE/2;
	
	

 	/* Start ADC1 Software Conversion */ 
 	ADC_Cmd(ADC1, ENABLE);
//	ADC_Cmd(ADC2, ENABLE);
	ADC_SoftwareStartConv(ADC1);
   LED_ON;
	while(!DMA_GetFlagStatus(DMA2_Stream0, DMA_FLAG_TCIF0));
 	DMA_ClearFlag(DMA2_Stream0, DMA_FLAG_TCIF0);	
  	/* Enable ADC1 **************************************************************/
	LED_OFF;
	ADC_Cmd(ADC1, DISABLE);



	ADC_SUM[ ADDR] = 0;
	for(i=0; i < ADC_BUFFER_SIZE  ; i++){
		ADC_SUM[ADDR ] += (int)(*( pADC_BASE + i )  );
//		*(pdBData + i ) = (u8)((*( pADC_BASE + i )-1800) >> 2);
		}
	ADC_AVG = ( ADC_SUM[ADDR]/ADC_BUFFER_SIZE );	

	
// Mixer Down

	for(i=0; i < ADC_BUFFER_SIZE ; i++)
	{
		re = cos( (System.fFREQ + System.fDelta) *(Flo32)i );
		im = -sin( (System.fFREQ + System.fDelta) *(Flo32)i);
		fReal[i]	= ((Flo32)(*(pADC_BASE + i )) - (float)ADC_AVG) * re;//real[i];  // 
		fImag[i]  =  ((Flo32)(*(pADC_BASE + i )) - (float)ADC_AVG) * im;//* imag[i] ;// 메모리 부족으로 pData를 다시 활용 
		}
	
// FIR LPF16		

	for(i=0; i < ADC_BUFFER_SIZE  -DECI + 1; i++)
	{	

		fReal[i]  = fReal[i] * LPF16[0];
		fImag[i] = fImag[i] * LPF16[0];		
		for ( j=1;j<16;j++){
			fReal[ i ]   += fReal[ i + j ] * LPF16[j];
			fImag[ i ]  += fImag[ i + j ] * LPF16[j];

			}
		}

// Decimation 16

	for ( i = 0,j=0; i < ADC_BUFFER_SIZE  / DECI - 1;i++, j+=2){
		fReal[ i ]  = fReal[  i*DECI ];
		fImag[ i ] = fImag[  i*DECI ];		

		for ( n= 1;n<DECI;n++){
			fReal[i]  += fReal[i*DECI+n];
			fImag[i] += fImag[i*DECI+n];
			}
		*(pData+j) 	= fReal[i];
		*(pData+j+1) = fImag[i];
//		*(pdBData + i ) = fReal[i];
//		*(pdBData + i + ADC_BUFFER_SIZE  / DECI ) = fImag[i];
		}

	nNextStep = TRUE;
	return nNextStep;
	
}
#if 1
u8 Display_Data( void )
{
	char		strBuff[24];
	u16 		i;
	
	LCD_Clear(RGB_CYAN);

#if 1
	for ( i = 0; i < ADC_BUFFER_SIZE  / DECI - 1;i++){
		LCD_DrawGraph(i,240-dBData[i]       , 240-dBData[i+1],RGB_BLACK);
		LCD_DrawGraph(i,480-dBData[i+ADC_BUFFER_SIZE  / DECI] , 480-dBData[i+ADC_BUFFER_SIZE  / DECI+1],Black);

		}

#else
	for ( i = 0;i<682-1;i++){
		LCD_DrawGraph(i,160-dBData[i+nOffset]       , 160-dBData[i+nOffset+1],RGB_BLACK);
		LCD_DrawGraph(i,320-dBData[i+nOffset+682] , 320-dBData[i+nOffset+683],RGB_BLACK);
		LCD_DrawGraph(i,480-dBData[i+nOffset+1364] , 480-dBData[i+nOffset+1365],Black);

//		LCD_DrawGraph(i,100-dBData[i+960] , 100-dBData[i+961],Magenta);
//		LCD_DrawGraph(i,180-dBData[i+1280] , 180-dBData[i+1281],Magenta);
//		LCD_DrawGraph(i,260-dBData[i+1600] , 260-dBData[i+1601],Magenta);		
		}
#endif
	sprintf(strBuff,"AVG=%03d: SUM=%05d",ADC_AVG, ADC_SUM[1]);
	LCD_String_Write(500,200,strBuff,10,16,RGB_BLACK,RGB_CYAN,NORMAL);

	nNextStep = TRUE;
	return nNextStep;
}

#endif

//Flo32	ftemp[ADC_FFT_SIZE *2];

u8 FFT_CONV( u8 ADDR )
{
	int 		i,j;
	double 	fMag;
	Flo32	real,imag;
	Flo32 	ftemp[ADC_FFT_SIZE*2 ];	// FFT_SIZE : 2048
	Flo32	*pData, *pTemp;
	u8		*pdBData;
	
	pData 	= (void*)&ADC_Data;
	pdBData  = ( void *)&dBData;
	pTemp 	= ( void *)&ftemp;
	memset(pTemp,0,sizeof(ftemp));
	if ( ADDR ){
		pdBData += ADC_FFT_SIZE /2;
//		pData 	+= ADC_FFT_SIZE /2;
		}

	for (i=0, j=0;j<ADC_BUFFER_SIZE / DECI;i++, j+=2 ){
		*(pTemp + j ) = (Flo32)(*(pData + j )) * Blackman2048[(u16)i *DECI];//*real; //Nuttall1024, Blackman,BlackmanHarris,BlackmanHarris2
		*(pTemp + j +1) = (Flo32)(*(pData + j+ 1 )) * Blackman2048[(u16)i *DECI];//*imag;
//		*(pdBData + i ) = (u8)(*(pTemp + j ) );
		}

//*************************** FFT
//	arm_cfft_f32(&arm_cfft_sR_f32_len4096, temp, FFT, 1);		// fft
	arm_cfft_f32(&arm_cfft_sR_f32_len2048, pTemp, FFT, 1);		// fft
//	arm_cfft_f32(&arm_cfft_sR_f32_len1024, ftemp, FFT, 1);		// fft

	System.fdBMax[ADDR] = 0;
	System.nIndexMax[ADDR] = 0;
	Motor.nResult[ADDR] = FALSE;
	Motor.nError[ADDR] = 0;
	
	for(i=0;i<ADC_FFT_SIZE/2 ;i++){
		real 	= *pTemp++;
		imag = *pTemp++;
		fMag = real *real + imag * imag;
		*(pData + i ) =  30.f * log10f( fMag )-100;	
		if ( *(pData + i) > System.fdBMax[ADDR] ){
			System.fdBMax[ADDR] = *(pData+i);
			System.nIndexMax[ADDR] = i;

			if ( System.nMenu == MENU_MEASURE  && *(pData + i) >  Motor.fAvgValue[ADDR] - 20 )
				Motor.nResult[ADDR] = TRUE;
			}
#if 1
		if ( *(pData+i) >0 && *(pData+i) < 250)
			*(pdBData + i ) = (u8)(*(pData + i ));
		else if ( *(pData + i) > 250)
			*(pdBData + i ) = 250;
		else
			*(pdBData + i ) = 1;
#endif
		}


	if ( System.nMenu == MENU_SOUND_CHECK ){
		Motor.fCheckValue[ADDR][Motor.nCNT] = System.fdBMax[ADDR];
		Motor.nCheckIndex[ ADDR][Motor.nCNT] = System.nIndexMax[ADDR];

		}
	else{
		if ( Motor.nResult[ADDR] ){

			Motor.fAvgValue[ADDR] = (Motor.fAvgValue[ADDR] * (Flo32)Motor.nCNT + System.fdBMax[ADDR] ) /(Flo32)(Motor.nCNT + 1 );
			Motor.fdBValue[ADDR][Motor.nCNT] = System.fdBMax[ADDR];
		
			}
		else
			Motor.nError[ADDR] = 1;

		if ( ADDR )
			if( Motor.nError[0] || Motor.nError[1] )
				Motor.nErrorCNT++;

		}
	
	nNextStep = TRUE;
	return nNextStep;
//*************************** Change to dB Scale 

}

u8 CheckMotor( u8 ADDR )
{
#if 0
	u16 		i;
	Flo32	*pData;

	
	pData 	= (void*)&ADC_Data;
	System.fdBMax = 0;
	System.nIndexMax = 0;
	Motor.nResult[ADDR] = FALSE;
	Motor.nError[ADDR] = 0;
	if ( ADDR )
		pData += ADC_FFT_SIZE/2;
		
	for(i=0;i<ADC_FFT_SIZE/2 ;i++){

		if ( *(pData + i) > System.fdBMax ){
			System.fdBMax = *(pData+i);
			System.nIndexMax = i;

			if ( System.nMenu == MENU_MEASURE  && *(pData + i) >  Motor.fAvgValue[ADDR] - 20 )
				Motor.nResult[ADDR] = TRUE;
			}

		
		}


	if ( System.nMenu == MENU_SOUND_CHECK ){
		Motor.fCheckValue[ADDR][Motor.nCNT] = System.fdBMax;
		Motor.nCheckIndex[ ADDR][Motor.nCNT] = System.nIndexMax;

		}
	else{
		if ( Motor.nResult[ADDR] ){

			Motor.fAvgValue[ADDR] = (Motor.fAvgValue[ADDR] * (Flo32)Motor.nCNT + System.fdBMax ) /(Flo32)(Motor.nCNT + 1 );
			Motor.fdBValue[ADDR][Motor.nCNT] = System.fdBMax;
		
			}
		else
			Motor.nError[ADDR] = 1;

		if ( ADDR )
			if( Motor.nError[0] || Motor.nError[1] )
				Motor.nErrorCNT++;

		}
	
#endif
	if( System.nMenu == MENU_SOUND_CHECK && Motor.nCNT )
		Display_SoundCheck( ADDR );

	nNextStep = TRUE;
	return TRUE;
	
}


void Calcu_STD_Dev( void )
{
	u16	i;
	Flo32 fSTDValue[2] = {0, 0};
	
	for ( i = 0;i< Motor.nCNT;i++ ){
		fSTDValue[0] += fabs( Motor.fAvgValue[0] - Motor.fdBValue[0][i] ) ;
		fSTDValue[1] += fabs( Motor.fAvgValue[1] - Motor.fdBValue[1][i] );
		}
	Motor.fSTDDEV[0] =fSTDValue[0] / (Flo32)Motor.nCNT;
	Motor.fSTDDEV[1] = fSTDValue[1] /(Flo32)Motor.nCNT;
}

void Display_FFT( void )
{


}

static _Bool	nKeyStatus = 0 , OldKeyStatus = 0;
static _Bool Key_Check(void)
{	
	
	if ( GPIO_ReadInputDataBit(USER_BUTTON_GPIO_PORT,USER_BUTTON_PIN))
		nKeyStatus = TRUE;
	else{
		nKeyStatus = FALSE;
		OldKeyStatus = FALSE;
		}
	
	if ( nKeyStatus != OldKeyStatus && nKeyStatus ){
		OldKeyStatus = nKeyStatus;
		return 1;
		}
	
	return 0;
	
}

void EXTI_Config(void)
{
	/* Set variables used */
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Enable clock for GPIOD */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* configuration PA4 as Input Mode */
	GPIO_InitStructure.GPIO_Pin 	= Open_TP_IRQ_PIN;
  	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_IN;
  	GPIO_InitStructure.GPIO_PuPd 	= GPIO_PuPd_DOWN;	//GPIO_PuPd_Down;
  	GPIO_Init(Open_TP_IRQ_PORT, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource4);

	EXTI_StructInit(&EXTI_InitStructure);
	EXTI_InitStructure.EXTI_Line =  EXTI_Line4 ;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = EXTI4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

}


void EXTI4_IRQHandler(void)
{


	EXTI_ClearITPendingBit(EXTI_Line4);
	do
	   	{
		    	nPtr=Read_XTP2046();
		   	}while( nPtr == (void*)0 );

}

void Time_Update(void)
{
	LocalTime_t += SYSTEMTICK_PERIOD_MS;
}

void TimingDelay_Decrement(void)
{
  	if (TimingDelay != 0x00)
  	{ 
    		TimingDelay--;
  	}
}

// Delay Sum => 300
#define	DELAY1  30
#define	DELAY2	150

void Measure_Motor( void )
{

	switch( MotorTest ){
		case	MEASURE_READY:
				if ( System.nMenu ==  MENU_SOUND_CHECK || System.nMenu ==  MENU_MEASURE){
					START_ON;
					MotorTest = DELAY_10msec ;
					nStepCNT = 0;
					}
				break;

		case	DELAY_10msec: // 20msec
				if( ++nStepCNT >=DELAY1 + System.nDelay ){
					MotorTest = ADC1_Input;
					nNextStep = FALSE;
					ADC_INPUT( 0 );
					}
				break;

		case	ADC1_Input:
				if ( nNextStep ){
					MotorTest = RELAY_OFF;
					START_OFF;
					nNextStep = FALSE;
					FFT_CONV( 0 );
					}
				break;
		case	DISPLAY_DATA:
				if ( Motor.nCNT ){
					nNextStep = FALSE;
					Display_Data();
					MotorTest = DISPLAY_DELAY;
					}
				else
					MotorTest = RELAY_OFF;
				break;
		case	DISPLAY_DELAY:
				if ( nNextStep ){
					MotorTest = RELAY_OFF;
					
					}
				break;
				
		case	RELAY_OFF:
				if ( nNextStep ){
					MotorTest = DELAY_150msec;
					nStepCNT = 0;
					nNextStep = FALSE;
					}

				break;

		case	DELAY_150msec:
				if( ++nStepCNT >=DELAY2 ){
				
					MotorTest = ADC2_Input;
					nNextStep = FALSE;
					ADC_INPUT( 1 );
					}
				break;

		case	ADC2_Input:
				if ( nNextStep ){

					MotorTest = DELAY_1msec;
					nNextStep = FALSE;
					nStepCNT = 0;
					FFT_CONV( 1 );
					}
					
				break;	
		case	DELAY_1msec:
				if ( ++nStepCNT >= 20 ){
					MotorTest = FFT1_START;

					}
		
		case	FFT1_START:

				if( nNextStep ){
					MotorTest = ANALYSIS1;
					nStepCNT = 0;
					nNextStep =  FALSE;
					CheckMotor( 0 );

					}
				break;
				
		case	ANALYSIS1:
				if( nNextStep ){
					
					MotorTest = FFT2_START;
					
					}
				break;

				
		case	FFT2_START:
				if ( nNextStep ){
					MotorTest = ANALYSIS2;
					nNextStep =  FALSE;
					CheckMotor( 1 );
					}
				break;
				
		case	ANALYSIS2:
				if ( nNextStep ){

					if ( System.nMenu == MENU_SOUND_CHECK && Motor.nCNT >= 1 && Motor.nCNT <= 2){

						nNextStep = FALSE;
						FindFreqSound( );
						MotorTest = DELAY_1500msec;
						Motor.nCNT++;
						nStepCNT = 0;
						System.OnRefresh = TRUE;						
						}

					else if ( System.nMenu == MENU_SOUND_CHECK && Motor.nCNT == CHECK_NUM - 1 ){
						MotorTest = CHECK_SOUND;
						nNextStep = FALSE;
						CheckSound( );
						}
					else {
						MotorTest = DELAY_1500msec;
						Motor.nCNT++;
						nStepCNT = 0;
						System.OnRefresh = TRUE;
						}
					}
				break;
		case	CHECK_SOUND:
				
				if( nNextStep ) {
					System.nLastMenu = System.nMenu;
					System.nMenu = MENU_SOUND_OK;
//					System.nMenu = MENU_MEASURE;

					System.OnRefresh = TRUE;
					MotorTest = DELAY_1500msec;
					nStepCNT = 0;
					Motor.nCNT = 0;
					}
				else{
					System.nLastMenu = System.nMenu;
					System.nMenu = MENU_SOUND_FAIL;
					MotorTest = ALL_DONE;
					nStepCNT = 0;
					Motor.nCNT = 0;					
					}

				break;	
	
		case	DELAY_1500msec:
				if( ++nStepCNT >= 1500 )
					MotorTest = CYCLE_DONE;
				break;
		case	CYCLE_DONE:
				if ( Motor.nCNT < Motor.nTotal )
					MotorTest = MEASURE_READY;
				else{
					MotorTest = ALL_DONE;
					System.nMenu = MENU_READY;
					System.OnRefresh = TRUE;
					}
				break;
		case	ALL_DONE:
 //				System.nMenu = MENU_READY;
				break;

		}

}
void FindFreqSound( void )
{	

	if ( Motor.fCheckValue[0][Motor.nCNT] >150 && Motor.nCheckIndex[0][Motor.nCNT] >=  10 && Motor.nCheckIndex[0][Motor.nCNT] <=  ADC_FFT_SIZE /2 - 10) {
		System.fDelta = (Flo32)( Motor.nCheckIndex[0][Motor.nCNT] -250 ) / 2610.f;
		System.fOffset = System.fDelta * 43115.78947368421 /(2*PI );
		Motor.nCNT = 2;
		}
	else{
		System.nDelay = 100;
		}
	nNextStep = TRUE;
}

u8 CheckSound( void ){

	u8	nResult = TRUE;
	u16	 i, j;

	for ( j= 0;j<2;j++){
		Motor.fAvgValue[j] = 0;
		Motor.fAvgIndex[j] = 0;
		}
	for ( i = CHECK_NUM - 3;i<CHECK_NUM;i++ )
		for ( j = 0;j<2;j++)
			if ( i ) {
				Motor.fAvgValue[j] += Motor.fCheckValue[j][i];
				Motor.fAvgIndex[j] += (Flo32)Motor.nCheckIndex[j][i];
				if ( Motor.fCheckValue[j][i] <= REFERENCE_LEVEL )
					nResult = FALSE;

				}

	Motor.fAvgIndex[0] /= 3;
	Motor.fAvgIndex[1] /= 3;
	Motor.fAvgValue[0] /= 3;
	Motor.fAvgValue[1] /= 3;

	for ( i = CHECK_NUM - 3;i<CHECK_NUM;i++){
		if ( fabs(Motor.fAvgValue[0] - ( Motor.fCheckValue[0][i] )) > 20 )
			nResult = FALSE;
		if ( fabs(Motor.fAvgValue[1] - ( Motor.fCheckValue[1][i] )) > 20 )
			nResult = FALSE;

		if ( fabs(Motor.fAvgIndex[0] - (Flo32)( Motor.nCheckIndex[0][i] )) > 5 )
			nResult = FALSE;
		if ( fabs(Motor.fAvgIndex[1] - (Flo32)( Motor.nCheckIndex[1][i] )) > 5 )
			nResult = FALSE;	
		}

	Display_Sound_Error( nResult);
	nNextStep = nResult;
	return nNextStep;
	
}
void SysTick_Handler(void)
{

	Time_Update();
	TimingDelay_Decrement();

#if 1

	Measure_Motor();

	if ( System.nStatus >> 4 == BUTTON_OPEN && System.nStatus & 0x0F == BUTTON_PRESSED )
		System.nButton = System.nTempButton;

#endif		


}

static void SysTick_Configuration(void)
{
	if (SysTick_Config(SystemCoreClock / CNT_1msec))
  	{ 
    		while (1);
  	}
	NVIC_SetPriority(SysTick_IRQn, 0x0);
}

/**
  * @}
  */ 

/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
