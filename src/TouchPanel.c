/*********************************************************************************************************
*
* File                : TouchPanel.c
* Hardware Environment: 
* Build Environment   : RealView MDK-ARM  Version: 4.20
* Version             : V1.0
* By                  : 
*
*                                  (c) Copyright 2005-2011, WaveShare
*                                       http://www.waveshare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "TouchPanel.h"
#include "config.h"
#include "LCD.h"
#include "Display.h"
#include "Protocol.h"
#include "key_process.h"
//#include "Font12X24.h"
#include "rgb_color_table.h"
#include "main.h"

/* Private variables ---------------------------------------------------------*/
Matrix matrix={1,1,50,50} ;
Coordinate  display ;


Coordinate ScreenSample[3];

Coordinate DisplaySample[3] = {
                                { 50,  50 },
								{ 750, 430},
                                { 50,430}
	                          };

/* Private define ------------------------------------------------------------*/
#define THRESHOLD 2


/*******************************************************************************
* Function Name  : ADS7843_SPI_Init
* Description    : 
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
static void XPT2046_SPI_Init(void) 
{ 
  SPI_InitTypeDef SPI_InitStruct;	

  RCC_APB1PeriphClockCmd(Open_RCC_SPI,ENABLE);	

  SPI_I2S_DeInit(Open_SPI);
  /* Open_SPI Config -------------------------------------------------------------*/ 
  SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex; 
  SPI_InitStruct.SPI_Mode = SPI_Mode_Master; 
  SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b; 
  SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low; 
  SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge; 
//  SPI_InitStruct.SPI_NSS = SPI_NSS_Soft; 
  SPI_InitStruct.SPI_NSS = SPI_NSS_Hard; 
//  SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16; 
 SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64; 

  SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB; 
  SPI_InitStruct.SPI_CRCPolynomial = 7; 

  SPI_Init(Open_SPI, &SPI_InitStruct);
  SPI_SSOutputCmd(Open_SPI, ENABLE);
  SPI_Cmd(Open_SPI, ENABLE); 

  Power_Down();
} 

/*******************************************************************************
* Function Name  : TP_Init
* Description    : 
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void TP_Init(void) 
{ 

	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(Open_SPI_SCK_GPIO_CLK | Open_SPI_MISO_GPIO_CLK | Open_SPI_MOSI_GPIO_CLK  | Open_SPI_NSS_GPIO_CLK,ENABLE);

	RCC_AHB1PeriphClockCmd(Open_TP_CS_CLK | Open_TP_IRQ_CLK,ENABLE);
	Open_SPI_CLK_INIT(Open_RCC_SPI,ENABLE);

	GPIO_PinAFConfig(Open_SPI_SCK_GPIO_PORT,  Open_SPI_SCK_SOURCE,  Open_GPIO_AF_SPI);
	GPIO_PinAFConfig(Open_SPI_MISO_GPIO_PORT, Open_SPI_MISO_SOURCE, Open_GPIO_AF_SPI);
	GPIO_PinAFConfig(Open_SPI_MOSI_GPIO_PORT, Open_SPI_MOSI_SOURCE, Open_GPIO_AF_SPI);
	GPIO_PinAFConfig(Open_SPI_NSS_GPIO_PORT, Open_SPI_NSS_SOURCE, Open_GPIO_AF_SPI);

	GPIO_InitStructure.GPIO_Pin = Open_SPI_SCK_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;  
	GPIO_Init(Open_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = Open_SPI_MISO_PIN;
	GPIO_Init(Open_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = Open_SPI_MOSI_PIN;
	GPIO_Init(Open_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = Open_SPI_NSS_PIN;
	GPIO_Init(Open_SPI_NSS_GPIO_PORT, &GPIO_InitStructure);

 #if 1 
    ///*TP_IRQ /
      GPIO_InitStructure.GPIO_Pin = Open_TP_IRQ_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN ;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(Open_TP_IRQ_PORT, &GPIO_InitStructure);	

#endif
  //	TP_CS(1); 
    	XPT2046_SPI_Init(); 

} 


/*******************************************************************************
* Function Name  : DelayUS
* Description    : 
* Input          : - cnt:
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/

static void DelayUS(vu32 cnt)
{
  uint16_t i;
  for(i = 0;i<cnt;i++)
  {
     uint8_t us = 12;
     while (us--)
     {
       ;   
     }
  }
}

#ifdef MANUAL
/*******************************************************************************
* Function Name  : SPI_Write_Byte
* Description    : 
* Input          : - cmd: 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
static void SPI_Write_Byte(uint8_t data)
{
	for(u8 i = 0; i < 8; i++)
	{
		if (data & 0x80)
		{
			SPI_MOSI_H();
		}
		else
		{
			SPI_MOSI_L();
		}
		data = data << 1;
		SPI_CLK_L();
		SPI_CLK_H();
		
	}

}

/*******************************************************************************
* Function Name  : SPI_Read_Byte
* Description    : 
* Input          : - cmd: 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
u8	InValue;
static u8 SPI_Read_Byte(void)
{
	u8 i, ret ;
	ret = 0;
	i = 8;

	do {
		i--;
		SPI_CLK_L();
		InValue = SPI_MISO();
		if (InValue)
		{
			//set the bit to 0 no matter what
			ret |= (1 << i);
		}

		SPI_CLK_H();
	} while (i > 0);
// Test Touch Input Value


	return ret;
	
}

#endif

/*******************************************************************************
* Function Name  : WR_CMD
* Description    : 
* Input          : - cmd: 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/


static void WR_CMD (uint8_t cmd)  
{ 

#if 0
//	SPI_CS_L();
	SPI_Write_Byte( cmd);
	DelayUS(100);
	SPI_Read_Byte();
//	SPI_CS_H();
#else

  /* Wait for SPI3 Tx buffer empty */ 
  while (SPI_I2S_GetFlagStatus(Open_SPI, SPI_I2S_FLAG_TXE) == RESET); 
  /* Send SPI3 data */ 
  SPI_I2S_SendData(Open_SPI,cmd); 
  /* Wait for SPI3 data reception */ 
  while (SPI_I2S_GetFlagStatus(Open_SPI, SPI_I2S_FLAG_RXNE) == RESET); 
  /* Read Open_SPI received data */ 
  SPI_I2S_ReceiveData(Open_SPI); 
  #endif
} 

void Power_Down( void )
{

	WR_CMD( 0x00 );

}

/*******************************************************************************
* Function Name  : RD_AD
* Description    : 
* Input          : None
* Output         : None
* Return         : 
* Attention		 : None
*******************************************************************************/

static int RD_AD(void)  
{ 
u16	buf=0;
u8 temp=0;
#if 0
//	SPI_CS_L();
	SPI_Write_Byte( 0x00);
	DelayUS(100);
	temp = SPI_Read_Byte();
	buf = temp;
	DelayUS(1000);
	SPI_Write_Byte( 0x00);
	DelayUS(100);
	temp = SPI_Read_Byte();	
  	buf |= temp<<8; 
//	SPI_CS_H();

#else
//  unsigned short buf,temp; 
  /* Wait for Open_SPI Tx buffer empty */ 
  while (SPI_I2S_GetFlagStatus(Open_SPI, SPI_I2S_FLAG_TXE) == RESET); 
  /* Send Open_SPI data */ 
  SPI_I2S_SendData(Open_SPI,0x0000); 
  /* Wait for SPI1 data reception */ 
  while (SPI_I2S_GetFlagStatus(Open_SPI, SPI_I2S_FLAG_RXNE) == RESET); 
  /* Read Open_SPI received data */ 
  temp=(u8)SPI_I2S_ReceiveData(Open_SPI); 
  buf=(u16)temp<<8; 
 // buf=temp; 
  DelayUS(1); 
  while (SPI_I2S_GetFlagStatus(Open_SPI, SPI_I2S_FLAG_TXE) == RESET); 
  /* Send Open_SPI data */ 
  SPI_I2S_SendData(Open_SPI,0x0000); 
  /* Wait for Open_SPI data reception */ 
  while (SPI_I2S_GetFlagStatus(Open_SPI, SPI_I2S_FLAG_RXNE) == RESET); 
  /* Read Open_SPI received data */ 
  temp=(u8)SPI_I2S_ReceiveData(Open_SPI); 

  
//  buf |= temp<<8; 
   buf |= (u16)temp;
   buf >>= 4;
   buf &= 0xffff;
#endif
  return buf; 
} 


/*******************************************************************************
* Function Name  : Read_X
* Description    : Read ADS7843 ADC X 
* Input          : None
* Output         : None
* Return         : 
* Attention		 : None
*******************************************************************************/
int Read_X(void)  
{  
#if 0


	DelayUS(1); 

	SPI_Write_Byte(CHX); 
   	DelayUS(100); 
//	while( !SPI_IRQ());
 	temp = SPI_Read_Byte(); 
 	SPI_Write_Byte(0x00);
	buf =  SPI_Read_Byte(); 
 	SPI_Write_Byte(0x00);
	temp = SPI_Read_Byte(); 

	return temp;
#else
  WR_CMD(CHX); 
  return RD_AD(); 
 #endif
} 

/*******************************************************************************
* Function Name  : Read_Y
* Description    : Read ADS7843 ADC Y
* Input          : None
* Output         : None
* Return         : 
* Attention		 : None
*******************************************************************************/
int Read_Y(void)  
{  
#if 0

  	SPI_Write_Byte(CHY); 
   	DelayUS(10); 
 //	while( !SPI_IRQ());
 	temp = SPI_Read_Byte(); 
 	SPI_Write_Byte(0x00);
	buf =  SPI_Read_Byte(); 
 	SPI_Write_Byte(0x00);
	temp = SPI_Read_Byte(); 
  	return temp;
#else
	WR_CMD(CHY); 
  	return RD_AD();   
#endif
} 


/*******************************************************************************
* Function Name  : TP_GetAdXY
* Description    : Read ADS7843
* Input          : None
* Output         : None
* Return         : 
* Attention		 : None
*******************************************************************************/
void TP_GetAdXY(int *x,int *y)  
{ 

  int adx,ady; 

  DelayUS(1); 
  adx=Read_X(); 
  DelayUS(1); 
  ady=Read_Y(); 
  DelayUS(1); 

  *x=adx; 
  *y=ady; 
} 


/*******************************************************************************
* Function Name  : DrawCross
* Description    : 
* Input          : - Xpos: Row Coordinate
*                  - Ypos: Line Coordinate 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void DrawCross(uint16_t Xpos,uint16_t Ypos)
{
	u16	xPos, yPos, xSize, ySize;
	u16	Color;

	xSize = 24;ySize = 24;
	Color = RGB_RED;
	xPos = Xpos - xSize/2; yPos = Ypos - ySize /2;
	LCD_Draw_box( xPos, yPos , xSize+1 , ySize, Color);
	LCD_Draw_box( xPos+1, yPos+1 , xSize-1 , ySize-2, Color);
	
	LCD_Draw_xline(xPos +3, Ypos , xSize - 5, Color);
	LCD_Draw_yline( Xpos , yPos + 3, ySize - 5, Color);		
}	
void Touch_Read( void )
{
	u16	nCNT = 100;
	do
	   	{
		    	nPtr=Read_XTP2046();
			WR_CMD( 0x00);
			nCNT--;
		   	}while( nPtr == (void*)0 && nCNT );
	if (getDisplayPoint(&display,nPtr, &matrix ))
		Check_Touch();
}
/*******************************************************************************
* Function Name  : Read_XTP2046
* Description    : Get TouchPanel X Y
* Input          : None
* Output         : None
* Return         : Coordinate *
* Attention		 : None
*******************************************************************************/
//Coordinate *Read_Ads7846(void)

int m0, m1, m2;
int buffer[2][9]={{0},{0}};
int TP_X[1],TP_Y[1],temp[3]; 

Coordinate *Read_XTP2046(void)
{
  static Coordinate  screen;
//  int m0,m1,m2;
//  int TP_X[1],TP_Y[1],temp[3];
  uint8_t count=0;
//  int buffer[2][9]={{0},{0}};

  do
  {		   
    TP_GetAdXY(TP_X,TP_Y);  
	buffer[0][count]=TP_X[0];  
	buffer[1][count]=TP_Y[0];
	count++;  

  	}while(!TP_INT_IN&& count<9);  /* TP_INT_IN  */

  if(count==9)   /* Average X Y  */ 
  {
	/* Average X  */
  	temp[0]=(buffer[0][0]+buffer[0][1]+buffer[0][2])/3;
	temp[1]=(buffer[0][3]+buffer[0][4]+buffer[0][5])/3;
	temp[2]=(buffer[0][6]+buffer[0][7]+buffer[0][8])/3;

	m0=temp[0]-temp[1];
	m1=temp[1]-temp[2];
	m2=temp[2]-temp[0];

	m0=m0>0?m0:(-m0);
 	m1=m1>0?m1:(-m1);
	m2=m2>0?m2:(-m2);

	if( m0>THRESHOLD  &&  m1>THRESHOLD  &&  m2>THRESHOLD ) return 0;

	if(m0<m1)
	{
	  if(m2<m0) 
	    screen.x=(temp[0]+temp[2])/2;
	  else 
	    screen.x=(temp[0]+temp[1])/2;	
	}
	else if(m2<m1) 
	  screen.x=(temp[0]+temp[2])/2;
	else 
	  screen.x=(temp[1]+temp[2])/2;

	/* Average Y  */
  	temp[0]=(buffer[1][0]+buffer[1][1]+buffer[1][2])/3;
	temp[1]=(buffer[1][3]+buffer[1][4]+buffer[1][5])/3;
	temp[2]=(buffer[1][6]+buffer[1][7]+buffer[1][8])/3;
	m0=temp[0]-temp[1];
	m1=temp[1]-temp[2];
	m2=temp[2]-temp[0];
	m0=m0>0?m0:(-m0);
	m1=m1>0?m1:(-m1);
	m2=m2>0?m2:(-m2);

	if(m0>THRESHOLD&&m1>THRESHOLD&&m2>THRESHOLD) return 0;
	
	if(m0<m1)
	{
	  if(m2<m0) 
	    screen.y=(temp[0]+temp[2])/2;
	  else 
	    screen.y=(temp[0]+temp[1])/2;	
    	}
	else if(m2<m1) 
	   screen.y=(temp[0]+temp[2])/2;
	else
	   screen.y=(temp[1]+temp[2])/2;

	return &screen;
  }  
  return 0; 
}
	 


	 

/*******************************************************************************
* Function Name  : setCalibrationMatrix
* Description    : Calculate K A B C D E F
* Input          : None
* Output         : None
* Return         : 
* Attention		 : None
*******************************************************************************/
FunctionalState setCalibrationMatrix( Coordinate * displayPtr,
                          Coordinate * screenPtr,
                          Matrix * matrixPtr)
{

  FunctionalState retTHRESHOLD = ENABLE ;

    matrixPtr->ScaleX = (double)( DISP_WIDTH -100 )/( screenPtr[1].x  -  screenPtr[0].x );
    matrixPtr->ScaleY = (double)( DISP_HEIGHT-100 )/( screenPtr[1].y  -  screenPtr[0].y );	

   if ( matrixPtr->ScaleX * matrixPtr->ScaleY ){
	    matrixPtr->BiasX = 50 -	matrixPtr->ScaleX * screenPtr[0].x;	
	    matrixPtr->BiasY = 50 -	matrixPtr->ScaleY * screenPtr[0].y;	

   	}
   else
   		retTHRESHOLD = DISABLE;
  return( retTHRESHOLD ) ;
}

/*******************************************************************************
* Function Name  : getDisplayPoint
* Description    : Touch panel X Y to display X Y
* Input          : None
* Output         : None
* Return         : 
* Attention		 : None
*******************************************************************************/
FunctionalState getDisplayPoint(Coordinate * displayPtr,
                     Coordinate * screenPtr,
                     Matrix * matrixPtr )
{
  FunctionalState retTHRESHOLD =ENABLE ;
  u16	 xPos, yPos;
  char	strBuff[32];

	if ( screenPtr == (void *) 0 ){
		displayPtr->x = DISP_WIDTH;displayPtr->y = 	DISP_HEIGHT;
		retTHRESHOLD = DISABLE;
		return( retTHRESHOLD );
		}

    /* XD = AX+BY */        
	xPos = (u16)( matrixPtr->ScaleX  * screenPtr->x + matrixPtr->BiasX) ; 
	if ( xPos > DISP_WIDTH ){
		retTHRESHOLD =DISABLE;
		displayPtr->x = DISP_WIDTH;
		}
	else
		displayPtr->x = xPos;
	/* YD = DX+EY+F */        
    	yPos = (u16)( matrixPtr->ScaleY * screenPtr->y + matrixPtr->BiasY)  ;
	if ( yPos > DISP_HEIGHT){
		retTHRESHOLD =DISABLE;
		displayPtr->y = 	DISP_HEIGHT;
		}
	else
		displayPtr->y = yPos;
	
  return(retTHRESHOLD);
} 




/*******************************************************************************
* Function Name  : delay_ms
* Description    : Delay Time
* Input          : - cnt: Delay Time
* Output         : None
* Return         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void delay_ms( uint16_t ms)    
{ 
	uint16_t i,j; 
	for( i = 0; i < ms; i++ )
	{ 
		for( j = 0; j < 0xffff; j++ );
	}
} 

/*******************************************************************************
* Function Name  : TouchPanel_Calibrate
* Description    : 
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void TouchPoint( void )
{
	u16	xPos, yPos;
	char	strBuff[32];
	
	 Coordinate * Ptr;
	 delay_ms(10);

	   do
	   {
//	     	TP_GetAdXY(Xp,Yp);
		Ptr = Read_XTP2046();
	   	}while( Ptr == (void*)0 );

//	sprintf(strBuff,"xData: %08X, yData: %08X",Xp[0], Yp[0]  );	   
	sprintf(strBuff,"xData: %05d, yData: %05d",Ptr->x, Ptr->y  );	   

	xPos = 480;yPos = 200;
	LCD_String_Write(xPos,yPos,strBuff,10,16,RGB_BLACK,RGB_YELLOW,NORMAL);		

}

void TouchPanel_Calibrate(void)
{

#if 1
	ScreenSample[0].x = 410;ScreenSample[0].y = 520;
//	ScreenSample[2].x = 1010;ScreenSample[2].y = 1420;
	ScreenSample[1].x = 1680;ScreenSample[1].y = 1510;
 #else
  uint8_t i;
  Coordinate * Ptr;
  
  LCD_Clear(RGB_BLUE);
  LCD_String_Write(100,20,"LCD Touch to calibrate",12,24, White,Black, TRANSPARENT);

  for(i=0;i<2;i++)
  {

	   delay_ms(100);
	   DrawCross(DisplaySample[i].x,DisplaySample[i].y);
	   do
	   {
	     	Ptr=Read_XTP2046();
	   	}while( Ptr == (void*)0 );
	   ScreenSample[i].x= Ptr->x; ScreenSample[i].y= Ptr->y;
 	}

 
  #endif
  setCalibrationMatrix( &DisplaySample[0],&ScreenSample[0],&matrix );
//  LCD_Clear(RGB_BLUE);
} 

void TouchPanel_NoCalibrate(void)
{
	memcpy(&ScreenSample[0],&DisplaySample[0], sizeof( DisplaySample ));
  	setCalibrationMatrix( &DisplaySample[0],&ScreenSample[0],&matrix );
  	LCD_Clear(RGB_BLUE);


}
/*********************************************************************************************************
      END FILE
*********************************************************************************************************/
