
#include "lcd.h"
#include "stdlib.h"
#include "main.h"
#include "rgb_color_table.h"
#include "Display.h"
#include "TouchPanel.h"

#include "fonts.h"
#include "Font12X24.h"


//������ɫ,������ɫ
u16 POINT_COLOR = 0x0000,BACK_COLOR = 0xFFFF;  
u16 DeviceCode;	 
/* Private variables ---------------------------------------------------------*/

unsigned int  HDP=799;
unsigned int  HT=525;
unsigned int  HPS=45;
unsigned int  LPS=8;
unsigned char HPW=10;

unsigned int  VDP=479;
unsigned int  VT=288;
unsigned int  VPS=16;
unsigned int  FPS=4;
unsigned char VPW=10;

//unsigned short Frame_Buf[480][272];

/*******************************************************************************
* Function Name  : LCD_CtrlLinesConfig
* Description    : Configures LCD Control lines (FSMC Pins) in alternate function
                   Push-Pull mode.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
static void LCD_CtrlLinesConfig(void)
{
	GPIO_InitTypeDef GPIO_InitStructure; 
	
	/* Enable GPIOs clock */
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE, ENABLE);
	
	/* Enable FSMC clock */
	RCC_AHB3PeriphClockCmd(RCC_AHB3Periph_FSMC, ENABLE); 

	/*-- GPIOs Configuration ------------------------------------------------------*/
	/*
	+-------------------+--------------------+------------------+------------------+
	+                       SRAM pins assignment                                   +
	+-------------------+--------------------+------------------+------------------+
	| PD0  <-> FSMC_D2  	| PE2  <-> FSMC_A23  	| PD13 <-> FSMC_A18(RS)	|
	| PD1  <-> FSMC_D3  	|                    		| PD7<-> FSMC_NE1 		|
	| PD4  <-> FSMC_NOE 	| PE7  <-> FSMC_D4   	| 						|
	| PD5  <-> FSMC_NWE 	| PE8  <-> FSMC_D5   	|------------------+ 
	| PD8  <-> FSMC_D13 	| PE9  <-> FSMC_D6   	| 
	| PD9  <-> FSMC_D14 	| PE10 <-> FSMC_D7   	|  
	| PD10 <-> FSMC_D15 	| PE11 <-> FSMC_D8   	| 
	|                   		| PE12 <-> FSMC_D9   	| 
	|                   		| PE13 <-> FSMC_D10 	|                   
	| PD14 <-> FSMC_D0  	| PE14 <-> FSMC_D11 	|                  
	| PD15 <-> FSMC_D1 	| PE15 <-> FSMC_D12 	|
	+-------------------+--------------------+
	*/
	/* GPIOD configuration */
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource0, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource1, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource4, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource7, GPIO_AF_FSMC);	// NE1
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource10, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource13, GPIO_AF_FSMC);	// RS
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource14, GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOD, GPIO_PinSource15, GPIO_AF_FSMC);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_7 |
	                            GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	
	
	/* GPIOE configuration */
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource2 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource7 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource8 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource9 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource10 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource11 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource12 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource13 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource14 , GPIO_AF_FSMC);
	GPIO_PinAFConfig(GPIOE, GPIO_PinSource15 , GPIO_AF_FSMC);
	
	GPIO_InitStructure.GPIO_Pin =GPIO_Pin_2 | GPIO_Pin_7 | GPIO_Pin_8  | GPIO_Pin_9  | GPIO_Pin_10 |
	                             GPIO_Pin_11| GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	
	GPIO_Init(GPIOE, &GPIO_InitStructure);


	RCC_AHB1PeriphClockCmd(LCD_RESET_RCC_CLK, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin 	  = LCD_RESET_PIN ;
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP; 
  	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  	GPIO_Init(LCD_RESET_GPIO_PORT, &GPIO_InitStructure);	


	
}

static void LCD_FSMCConfig(void)
{
	FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
	FSMC_NORSRAMTimingInitTypeDef FSMC_NORSRAMTimingInitStructure;
	
	FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM1;	//FSMC_Bank1_NORSRAM4;
	FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
	FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
	FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
	FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
	FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
	FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
	FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait = FSMC_AsynchronousWait_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &FSMC_NORSRAMTimingInitStructure;
	//FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure); 
	/* FSMC Write Timing */
	FSMC_NORSRAMTimingInitStructure.FSMC_AddressSetupTime = 15;	///2;	//15; //15;   /* Address Setup Time */
	FSMC_NORSRAMTimingInitStructure.FSMC_AddressHoldTime = 0;	   
	FSMC_NORSRAMTimingInitStructure.FSMC_DataSetupTime =15; ///2;	//15; 	//15;	     /* Data Setup Time */
	FSMC_NORSRAMTimingInitStructure.FSMC_BusTurnAroundDuration = 2;	///2;
	FSMC_NORSRAMTimingInitStructure.FSMC_CLKDivision = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_DataLatency = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_AccessMode = FSMC_AccessMode_A;	/* FSMC Access Mode */
	FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &FSMC_NORSRAMTimingInitStructure;	  
	
	FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure); 
	
	/* Enable FSMC Bank4_SRAM Bank */
	//FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM4, ENABLE); 
	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM1, ENABLE); 
}

static void LCD_FSMCConfig_2(void)
{
	FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
	FSMC_NORSRAMTimingInitTypeDef FSMC_NORSRAMTimingInitStructure;
	
	FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM1;	//FSMC_Bank1_NORSRAM4;
	FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable;
	FSMC_NORSRAMInitStructure.FSMC_MemoryType = FSMC_MemoryType_SRAM;
	FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;
	FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode = FSMC_BurstAccessMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
	FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;
	FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;
	FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;
	FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait = FSMC_AsynchronousWait_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Disable;
	FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable;
	FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &FSMC_NORSRAMTimingInitStructure;
	//FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure); 
	/* FSMC Write Timing */
	FSMC_NORSRAMTimingInitStructure.FSMC_AddressSetupTime = 15;	///2;	//15; //15;   /* Address Setup Time */
	FSMC_NORSRAMTimingInitStructure.FSMC_AddressHoldTime = 1;	   
	FSMC_NORSRAMTimingInitStructure.FSMC_DataSetupTime =10; ///2;	//15; 	//15;	     /* Data Setup Time */
	FSMC_NORSRAMTimingInitStructure.FSMC_BusTurnAroundDuration = 2;	///2;
	FSMC_NORSRAMTimingInitStructure.FSMC_CLKDivision = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_DataLatency = 0x00;
	FSMC_NORSRAMTimingInitStructure.FSMC_AccessMode = FSMC_AccessMode_A;	/* FSMC Access Mode */
	FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &FSMC_NORSRAMTimingInitStructure;	  
	
	FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure); 
	
	/* Enable FSMC Bank4_SRAM Bank */
	//FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM4, ENABLE);  
	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM1, ENABLE); 
}

static void LCD_Configuration(void)
{
	u32 i=0x1fffff;
	/* Configure the LCD Control pins --------------------------------------------*/
	LCD_CtrlLinesConfig();
	while(i--);
	/* Configure the FSMC Parallel interface -------------------------------------*/
	LCD_FSMCConfig();
}

static void TFTLCD_InitCode(void)
{        
    // Hardware Reset
	GPIO_ResetBits(LCD_RESET_GPIO_PORT, LCD_RESET_PIN);
	delay_ms(10);   
	GPIO_SetBits(LCD_RESET_GPIO_PORT, LCD_RESET_PIN);
	delay_ms(10);		// delay_ms(1000000);

    LCD_REG=(SET_DISPLAY_OFF);         //-- SET display on   

 #ifndef TFT_43
 // Set the PLL (PLL Frequency = 10MHz*36/3 = 120MHz)
    LCD_REG = (SET_PLL_MN);

    LCD_RAM =(35);           // N 36-1 : 0x23
    LCD_RAM =(2);            //  M 3-1	: 0x2
    LCD_RAM =(0x04);    


	// Start the PLL
    LCD_REG =(START_PLL);
    LCD_RAM =(0x03);    
    delay_ms(1);     
           
    // Software Reset
    LCD_REG =(0x01);
    delay_ms(100);    
   // Delay_ms(10);
	
    //-- To obtain PCLK = 40MHz with PLL Frequency = 120MHz,    
    //-- 9MHz = 120MHz * ( LCDC_FPR+ 1) / 2^20
    //-- LCDC_FPR = 0x40000    
    LCD_REG  =(SET_LSHIFT_FREQ);  // 24MHz= 120 * value /2^20
    LCD_RAM =(0x01);
    LCD_RAM =(0x99);
    LCD_RAM =(0x98); 

    LCD_REG  =(SET_LCD_MODE);         //-- SET LCD MODE  SET TFT 18Bits MODE   
 //   LCD_RAM =(0x24);         //0x24  	0x20: SET TFT 24Bits	0x04: Data latch in falling Edge
    LCD_RAM =(0x20);         //0x20  	0x20: SET TFT 24Bits	0x00: Data latch in rising Edge  // Sunlight Readible LCD�� ��� 
   LCD_RAM =(0x00);			// Hsync + Vsync + DE mode, TFT mode
    LCD_RAM =(0x01);         //-- SET horizontal size=480 -1 HightByte
    LCD_RAM =(0xDF);
    LCD_RAM =(0x01);         //-- SET vertical size=272 -1 HightByte
    LCD_RAM =(0x0F);
    LCD_RAM =(0x00);         //-- SET even/odd line RGB seq.=RGB
    
    LCD_REG  =(SET_PIXEL_FORMAT);         //-- SET pixel data I/F format=16bit (5 6 5 )//@@    
    LCD_RAM =(0x50);			// 0x50 : 16bit/pixel
    LCD_REG  =(SET_PIXEL_INTERFACE);         //-- SET pixel data I/F format=16bit (5 6 5 )
    LCD_RAM =(0x03);			// 16-bit ( 565 format )
    

	LCD_REG =(SET_HOR_PERIOD);          //-- SET HBP 
    	LCD_RAM =((520>>8)&0xFF);  	//Set HT
	LCD_RAM =(520&0xFF);
	LCD_RAM =((40>>8)&0xFF);  	//Set HPS
	LCD_RAM =(40&0xFF);
	LCD_RAM =(HPW);			   	//Set HPW
	LCD_RAM =((LPS>>8)&0xFF);  	//Set HPS
	LCD_RAM =(LPS&0xFF);
	LCD_RAM =(0x00);

	LCD_REG  =(SET_VERT_PERIOD);         //-- SET VBP 
	LCD_RAM =((277>>8)&0X00FF);   //Set VT
	LCD_RAM =(277&0X00FF);
	LCD_RAM =((5>>8)&0X00FF);  //Set VPS
	LCD_RAM =(5&0X00FF);
	LCD_RAM =(VPW);			   //Set VPW
	LCD_RAM =((FPS>>8)&0X00FF);  //Set FPS
	LCD_RAM =(FPS&0X00FF);

		
    	LCD_REG  =(SET_ADDR_MODE);         // horizontal:increment, vertical:increment
    	LCD_RAM =(SET_NORMAL_DISPLAY);		// 0x00 : Nomal Display

 
    LCD_REG  =(SET_COLUMN_ADDR);         //-- SET column address
    LCD_RAM =(0x00);         //-- SET start column address=0
    LCD_RAM =(0x00);
    LCD_RAM =(0x01);         //-- SET end column address=479
    LCD_RAM =(0xDF);
    
    LCD_REG  =(SET_PAGE_ADDR);         //-- SET page address
    LCD_RAM =(0x00);         //-- SET start page address=0
    LCD_RAM =(0x00);
    LCD_RAM =(0x01);         //-- SET end page address=271
    LCD_RAM =(0x0F);
    
    LCD_REG=(SET_DISPLAY_ON);         //-- SET display on   
    
    LCD_REG  =(SET_GPIO_CONF);         //-- SET GPIO Configure
    LCD_RAM =(0x0F);     			// GPIO0 ~3 Output controller by host
    LCD_RAM =(0x01);				// GPIO0 : used as normal GPIO
    
    LCD_REG	=(SET_GPIO_VALUE);
    LCD_RAM =(0x03);				// GPIO3 : 0 ,  GPIO2 : 0 ,  GPIO1 : 1 ,  GPIO0 : 1   
       
    LCD_REG	=(SET_DBC_TH);		//
    LCD_RAM =(0x00);
    LCD_RAM =(0x09);
    LCD_RAM =(0x90);
	
    LCD_RAM =(0x00);
    LCD_RAM =(0x17);
    LCD_RAM =(0xE8);

    LCD_RAM =(0x00); 
    LCD_RAM =(0x39);
    LCD_RAM =(0x60); 
	
	LCD_REG=(SET_DBC_CONF);
    	LCD_RAM =(0x0D);


#else

// Set the PLL (PLL Frequency = 10MHz*30/3 = 100MHz)
    LCD_REG = (SET_PLL_MN);
    LCD_RAM =(29);           // N 12-1 : 0x0B
    LCD_RAM =(2);            //  M 3-1	: 0x2
    LCD_RAM =(0x04);    


	// Start the PLL
    LCD_REG =(START_PLL);
    LCD_RAM =(0x03);    
    delay_ms(1);     
      // Software Reset
    LCD_REG =(0x01);
    delay_ms(1000);           
  
   
    LCD_REG  =(SET_LSHIFT_FREQ);  // 25MHz= 100 * value /2^20 => Value = 0x040000,12.5MHz  => Value = 0x020000
    LCD_RAM =(0x01);			// 10MHz = 100*value /2^20 => value = 0x019999
    LCD_RAM =(0x99);				//8.33MHz = 100* value /2^20 => value = 0x011111
    LCD_RAM =(0x98); 			//6.25MHz = 100* value /2^20 => value = 0x010000

	
    LCD_REG  =(SET_LCD_MODE);         //-- SET LCD MODE  SET TFT 18Bits MODE   
    for ( u8 i=0;i<7;i++)
	   LCD_RAM = nLCD_Mode[i];    

    
    LCD_REG  =(SET_PIXEL_FORMAT);         //-- SET pixel data I/F format=16bit (5 6 5 )//@@    
    LCD_RAM =(0x50);			// 0x50 : 16bit/pixel
    LCD_REG  =(SET_PIXEL_INTERFACE);         //-- SET pixel data I/F format=16bit (5 6 5 )
    LCD_RAM =(0x03);			// 16-bit ( 565 format )
    

	LCD_REG =(SET_HOR_PERIOD);          //-- SET HBP 
    	LCD_RAM =((928>>8)&0xFF);  	//Set HT
	LCD_RAM =(928&0xFF);
	LCD_RAM =((128>>8)&0xFF);  	//Set HPS
	LCD_RAM =(128&0xFF);
	LCD_RAM =(HPW);			   	//Set HPW
	LCD_RAM =((LPS>>8)&0xFF);  	//Set HPS
	LCD_RAM =(LPS&0xFF);
	LCD_RAM =(0x00);

	LCD_REG  =(SET_VERT_PERIOD);         //-- SET VBP 
	LCD_RAM =((520>>8)&0xFF);   //Set VT
	LCD_RAM =(520&0xFF);
	LCD_RAM =((40>>8)&0xFF);  //Set VPS
	LCD_RAM =(40&0xFF);
	LCD_RAM =(VPW);			   //Set VPW
	LCD_RAM =((FPS>>8)&0X00FF);  //Set FPS
	LCD_RAM =(FPS&0X00FF);


		
    	LCD_REG  =(SET_ADDR_MODE);         // horizontal:increment, vertical:increment
 //   	LCD_RAM =(SET_NORMAL_DISPLAY);		// 0x00 : Nomal Display
  	LCD_RAM =(0x01);//(SET_REVARSAL_DISPLAY);		// 0x03 : Top Bottom ���� 


    LCD_REG  =(SET_COLUMN_ADDR);         //-- SET column address
    LCD_RAM =(0x00);         //-- SET start column address=0
    LCD_RAM =(0x00);
    LCD_RAM =(0x03);         //-- SET end column address=799
    LCD_RAM =(0x1F);
    
    LCD_REG  =(SET_PAGE_ADDR);         //-- SET page address
    LCD_RAM =(0x00);         //-- SET start page address=0
    LCD_RAM =(0x00);
    LCD_RAM =(0x01);         //-- SET end page address=479
    LCD_RAM =(0xDF);


    LCD_REG=(SET_DISPLAY_ON);         //-- SET display on   
    
    LCD_REG  =(SET_GPIO_CONF);         //-- SET GPIO Configure
    LCD_RAM =(0x0F);     			// GPIO0 ~3 Output controller by host
    LCD_RAM =(0x01);				// GPIO0 : used as normal GPIO
    
    LCD_REG	=(SET_GPIO_VALUE);
    LCD_RAM =(0x03);				// GPIO3 : 0 ,  GPIO2 : 0 ,  GPIO1 : 1 ,  GPIO0 : 1   
       
    LCD_REG=(SET_PWM_CONF);
	LCD_RAM = (0x02);	// PWM signal Freq. = PLL clock / ( 256 * PWMF)/256
	LCD_RAM = (0x80);		// PWM Duty
	LCD_RAM = (0x01);	// PWM conf C3: 0 host, 1 DBC   C0=> PWM Enable : 1, Disable :0
	LCD_RAM = (0xFF);	
	LCD_RAM = (0x00);	// Minimum Brightness  00 ~ FF
	LCD_RAM = (0x0F);	// Brightness Prescaler  F: Brightest
    
		// GPIO3 : 0 ,  GPIO2 : 0 ,  GPIO1 : 1 ,  GPIO0 : 1   
       
    LCD_REG	=(SET_DBC_TH);		//
    LCD_RAM =(0x00);
    LCD_RAM =(0x1C);
    LCD_RAM =(0x20);	// TH1: Display Width * display height *3*0.1/16
	
    LCD_RAM =(0x00);
    LCD_RAM =(0x46);
    LCD_RAM =(0x50);	 // TH2: Display Width * display height *3*0.25/16

    LCD_RAM =(0x00); 
    LCD_RAM =(0xA8);
    LCD_RAM =(0xC0); 	// TH3: Display Width * display height *3*0.6/16
	
	LCD_REG=(SET_DBC_CONF);
//    	LCD_RAM =(0x0D);  // Aggressive mode
    	LCD_RAM =(0x0D);  // Normal mode


#endif


//    LCD_REG=(SET_DISPLAY_ON);         //-- SET display on   


}
void LCD_Initializtion(void)
{
	LCD_Configuration();
	TFTLCD_InitCode();
	LCD_FSMCConfig_2();
}


static void TFTLCD_SetAddr(u16 x1, u16 y1, u16 x2, u16 y2)
{

    	LCD_REG=(SET_COLUMN_ADDR);
    	LCD_RAM=( x1>>8);
    	LCD_RAM=(x1&0x00FF);
    	LCD_RAM=(x2>>8);
    	LCD_RAM=(x2&0x00FF);    
	
    	LCD_REG=(SET_PAGE_ADDR);
    	LCD_RAM=(y1>>8);
    	LCD_RAM=(y1&0x00FF);
    	LCD_RAM=(y2>>8);
    	LCD_RAM=(y2&0x00FF); 
 
}


/*****************************************************************************
 * @name       :void LCD_Clear(u16 Color)
 * @date       :2018-08-09 
 * @function   :Full screen filled LCD screen
 * @parameters :color:Filled color
 * @retvalue   :None
******************************************************************************/	
void LCD_Clear(u16 Color)
{
	LCD_Color(0, 0, DISP_WIDTH, DISP_HEIGHT, Color);	//RGB_WHITE); RGB_BLACK

} 

void LCD_Color(u16 xStart, u16 yStart, u16 xSize, u16 ySize, u16 Color)
{
    	u32 size = xSize*ySize;;

    	TFTLCD_SetAddr(xStart,yStart,xStart+xSize-1,yStart+ySize-1);
     	LCD_REG	=(WRITE_MEM_START); 
     	while(size--){
		LCD_RAM = Color;
//					LCD_RAM=(Color>> 8 & 0x0F8); 
//					LCD_RAM=(Color>> 3 & 0x0FC); 
//					LCD_RAM=(Color<< 3 & 0x0F8); 	
     		}
} 


/*****************************************************************************/


void LCD_Draw_box(u16 x1, u16 y1, u16 Width, u16 Height, u16 color)
{
	LCD_Draw_xline(x1, y1, Width, color);  
	LCD_Draw_xline(x1, y1+Height, Width, color);
	LCD_Draw_yline(x1, y1+1, Height, color);
	LCD_Draw_yline(x1+Width-1, y1+1, Height, color);
}
void LCD_Draw_xline(u16 x, u16 y,short dist, u16 color)
{								       
	
   	LCD_SetAddr(x,y,x+dist -1,y);
     	LCD_REG	=(WRITE_MEM_START);    
	while(dist--)
		LCD_RAM=(color); 	
} 

void LCD_Draw_yline(u16 x, u16 y,short dist, u16 color)
{								       
	
   	LCD_SetAddr(x,y,x,y+dist-1);
     	LCD_REG	=(WRITE_MEM_START);    
	while(dist--)
		LCD_RAM=(color); 	
} 

void LCD_SetAddr(u16 x1, u16 y1, u16 x2, u16 y2)
{

    	LCD_REG=(SET_COLUMN_ADDR);
    	LCD_RAM=(x1>>8);
    	LCD_RAM=(x1&0xFF);
    	LCD_RAM=(x2>>8);
    	LCD_RAM=(x2&0xFF);
    
    	LCD_REG=(SET_PAGE_ADDR);
    	LCD_RAM=(y1>>8);
    	LCD_RAM=(y1&0xFF);
    	LCD_RAM=(y2>>8);
    	LCD_RAM=(y2&0xFF);
 //    	LCD_REG	=(WRITE_MEM_START);      
}

static void LCD_Draw_point(uint16_t Xpos,uint16_t Ypos,uint16_t color)
{
	if( Xpos >= DISP_WIDTH|| Ypos >= DISP_HEIGHT)
	{
		return;
	}
	LCD_SetAddr(Xpos,Ypos,Xpos,Ypos);
	LCD_REG = WRITE_MEM_START;
	LCD_RAM = color;
	
//	LCD_WriteReg(0x0022,point);
}
void LCD_DrawGraph( u16 x0, u16 y0,  u16 y1 , u16 color )
{
  s16 dy;
  s16 temp;

  dy = (s16)y1-(s16)y0;

  if( dy == 0 )
  {
      LCD_Draw_point(x0, y0, color);
	return;
  	}
else if ( dy == 1 ){
	LCD_Draw_point(x0,y0,color);
	LCD_Draw_point(x0+1,y0+1,color);
}
else if ( dy == -1 ){
	LCD_Draw_point(x0,y0,color);
	LCD_Draw_point(x0+1,y0-1,color);

}
 else if( dy>1 )
  {
	for ( temp = 0;temp<= dy;temp++)
		if ( temp < dy/2 )
	    		LCD_Draw_point(x0,y0+temp,color);
		else
			LCD_Draw_point(x0+1,y0+temp,color);

  }  
  else
  {
	for ( temp = 0;temp>= dy;temp--)
		if ( temp > dy/2 )
	    		LCD_Draw_point(x0,y1+temp,color);
		else
			LCD_Draw_point(x0+1,y1+temp,color);
	}
} 

//u8 buffer[64], tmp, ASCII;
u16 nXPos, nYPos;


void LCD_String_Write(u16 xPos,u16 yPos,char *ch,u16 size, u16 Height, u16 text_color, u16 back_color, u16 MODE)
{
	while(*ch!='\0')
    	{   
      		LCD_Char_Write(xPos,yPos,*ch,size, Height, text_color, back_color, MODE);

	       xPos = xPos + size;  // Move to the next character position
	       ch++;
    }	

}

	u8	sIndex;
	u8	nText[16];
void  LCD_Char_Write(u16 xPos,u16 yPos,u8 nIndex,u16 size,u16 Height,u16 Text_color, u16 Back_color,  u16 MODE)
{
	u8	*pCh;
    	u8 	nPage,BitPos,xPoint,yPoint;      
	u16	xPos2,yPos2;

	u16 	i,  *pBack,	chBack[64*128];

	pBack = (void*)&chBack;
   	 if(xPos>DISP_WIDTH-size||yPos>DISP_HEIGHT -Height)
		return;

 	nIndex=nIndex-' ';
	sIndex = nIndex;
//	memset(&chBack,Back_color,sizeof(chBack));
	if ( MODE == TRANSPARENT ){
		LCD_Read_Image(xPos, yPos, size,Height,chBack );
		}
	else
		for ( i = 0;i < size * Height;i++)
			chBack[i] = Back_color;
	xPos2 = (u16)(xPos + (size - 1));
	yPos2 = (u16)(yPos + Height - 1);
  	TFTLCD_SetAddr(xPos,yPos,xPos2,yPos2); // 12 , 24

      	LCD_REG	=(WRITE_MEM_START); 

		switch(size){
			case	8:	pCh = (void*)&FONT8x16[nIndex];
						for ( i = 0;i<16;i++)
							nText[i]=FONT8x16[nIndex][i];
					break;
			case	9:	pCh = (void*)&FONT9x16[nIndex] ;
					break;
			case	10:	pCh = (void*)&FONT2_10x16[nIndex] ;
					break;
			case	12:	if ( Height == 16 ) pCh = (void*)&FONT12x16[nIndex] ;
					else if ( Height == 24 ) pCh = (void*)&FONT_12x24[nIndex] ;
					break;
			case	16:	if ( Height == 32 ) pCh = (void*)&FONT16x32[nIndex] ;
					break;
			default:	break;
			}

	for (yPoint = 0;yPoint<Height;yPoint++)		//���� 24bit  3 bytes
		{
			BitPos = yPoint % 8 ;
			nPage = (u8)( yPoint /8);	// 1byte , 1 Page = 8bits
			for(xPoint=0;xPoint<size ;xPoint++)		
		    	{                 

//				if( (chDISPLAY[ (Height/8) * xPoint + nPage]>>(BitPos)) & 0x001)

				if( (*(pCh + (Height/8) * xPoint + nPage ) >> BitPos ) & 0x01){
					LCD_RAM=(Text_color); 



					}
				else{
					LCD_RAM = *(pBack + xPoint + yPoint * size );
					
					}
					
		    	}
		}	
	

}

void	LCD_Read_Image(u16 Left, u16 Top, u16 Width,u16  Height,u16 *img )
{
	u16 	i;
	TFTLCD_SetAddr(Left,Top,Left+(Width-1),(Top+Height-1)); // 12 , 24
 	LCD_REG	=(READ_MEM_START);
	for ( i = 0;i < Width * Height;i++)
		img[i] = LCD_RAM;
}

void LCD_Show_Image(u16 xsta, u16 ysta, u16 xsize, u16 ysize, u16 *image ,u8 MODE)
{
	u16	 i;
	u32 size=xsize*ysize;

	u16	*pImg,	gImg[10000];

	pImg= (void *) &gImg;

	if ( MODE == TRANSPARENT ){
		LCD_Read_Image(xsta, ysta,xsize,ysize, gImg );
		for ( i= 0;i<size;i++)
			if ( *(image + i ) != RGB_WHITE )
				gImg[ i] = *(image + i);

		TFTLCD_SetAddr(xsta,ysta,xsta+xsize-1,ysta +ysize-1);
	     	LCD_REG	=(WRITE_MEM_START);   
		while(size--)	LCD_RAM= *pImg++;
			
		}

   	else{
		TFTLCD_SetAddr(xsta,ysta,xsta+xsize-1,ysta +ysize-1);
	     	LCD_REG	=(WRITE_MEM_START);    
		while(size--)	LCD_RAM=(*image++);
   		}


}

/*******************************************************************************
* Function Name  : delay_ms
* Description    : Delay Time
* Input          : - cnt: Delay Time
* Output         : None
* Return         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void Delay_ms(uint32_t ms)    
{ 
	uint32_t i,j; 
	for( i = 0; i < ms; i++ )
	{ 
		for( j = 0; j < 0xfffe; j++ );
	}
} 
